//
//  IntrestViewController.swift
//  Aidu
//
//  Created by varun murali on 14/07/17.
//  Copyright © 2017 Aidu. All rights reserved.
//

import UIKit

class IntrestViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    
    private var hobbyList = Hobby.getMockData()
    var profile = ProfileData()
    var ArtList = Intrest.getArtData();
    var FoodList = Intrest.getFoodData();
    var SportsList = Intrest.getSportsData();
    var MovieList = Intrest.getMovieData();
    var MyIntrest = Intrest.InitialData();
    var MusicList = Intrest.getMusicData();
    var TVList = Intrest.getTVData();
    var DesignList = Intrest.getDesignData();
    var ITList = Intrest.getITData();
    var BusinessList = Intrest.getBusinessData();
    var PoliticsList = Intrest.getPoliticsData();
    var HealthList = Intrest.getHealthData();
    var FashionList = Intrest.getFashionData();
    var LanguageList = Intrest.getLanguagesData();
    var TravelList = Intrest.getTravelData();
    var NatureList = Intrest.getNatureData();
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(profile.intrest.contains("Art")){
            for values in ArtList
            {
                MyIntrest.append(values);
            }
            
        }
        if(profile.intrest.contains("Music")){
            for values in MusicList
            {
                MyIntrest.append(values);
            }
            
        }
        if(profile.intrest.contains("Sports")){
            for values in SportsList
            {
                MyIntrest.append(values);
            }
            
        }
        if(profile.intrest.contains("TV Shows")){
            for values in TVList
            {
                MyIntrest.append(values);
            }
            
        }
        if(profile.intrest.contains("Design")){
            for values in DesignList
            {
                MyIntrest.append(values);
            }
            
        }
        if(profile.intrest.contains("Movies")){
            for values in MovieList
            {
                MyIntrest.append(values);
            }
            
        }
        if(profile.intrest.contains("IT & Tech")){
            for values in ITList
            {
                MyIntrest.append(values);
            }
            
        }
        if(profile.intrest.contains("Business")){
            for values in BusinessList
            {
                MyIntrest.append(values);
            }
            
        }
        if(profile.intrest.contains("Politics")){
            for values in PoliticsList
            {
                MyIntrest.append(values);
            }
        }
        if(profile.intrest.contains("Health")){
            for values in HealthList
            {
                MyIntrest.append(values);
            }
            
        }
        if(profile.intrest.contains("Fashion")){
            for values in FashionList
            {
                MyIntrest.append(values);
            }
            
        }
        if(profile.intrest.contains("Languages")){
            for values in LanguageList
            {
                MyIntrest.append(values);
            }
            
        }
        if(profile.intrest.contains("Travel")){
            for values in TravelList
            {
                MyIntrest.append(values);
            }
            
        }
        if(profile.intrest.contains("Food")){
            for values in FoodList
            {
                MyIntrest.append(values);
            }
            
        }
        if(profile.intrest.contains("Nature")){
            for values in NatureList
            {
                MyIntrest.append(values);
            }
            
        }
        
        
        //ArtButton.backgroundColor = UIColor.gray
        // Do any additional setup after loading the view.
    }
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    @IBAction func Next(_ sender: Any) {
        performSegue(withIdentifier: "next", sender: self)
        
    }
    
    
    @IBAction func Back(_ sender: Any) {
        performSegue(withIdentifier: "back", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "next" {
            let controller = segue.destination as! AboutViewController
            controller.profile = profile;
        }
        
        if segue.identifier == "back" {
            let controller = segue.destination as! HobbyViewController
            controller.profile = profile;
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return MyIntrest.count;
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Intrest", for: indexPath)
        
        if indexPath.row < MyIntrest.count
        {
            let item = MyIntrest[indexPath.row]
            cell.textLabel?.text = item.title
            if profile.intestList.contains(item.title){
                item.isIntrested = true
            }
            let accessory: UITableViewCellAccessoryType = item.isIntrested ? .checkmark : .none
            cell.accessoryType = accessory
        }
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row < MyIntrest.count
        {
            let item = MyIntrest[indexPath.row]
            item.isIntrested = !item.isIntrested;
            if profile.intestList.contains(item.title){
                //let str = item.title as String!
                print(item.title)
                let ind =  profile.intestList.index(of: item.title as String!) as Int!
                profile.intestList.remove(at: ind!)
            }
            else{ profile.intestList.append(item.title);}
            print(profile.intestList)
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }    
    
}
