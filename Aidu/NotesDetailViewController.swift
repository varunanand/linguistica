//
//  DetailViewController.swift
//  Plain Ol' Notes
//
//  Created by Todd Perkins on 12/6/16.
//  Copyright © 2016 Todd Perkins. All rights reserved.
//Just adding a new comment

import UIKit

class NotesDetailViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource {
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var categoryPicker: UIPickerView!
    var pickerLocation = 0;
    var type=["Grammar", "Vocabulary", "Phrase", "Slang"]
    var text:String = ""
    var masterView:NotesViewController!
    var message:String = ""
    var category:String = ""
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return type[row];
        
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return type.count;
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickerLocation=row;
        print("comes here")
        category = type[pickerLocation]
        self.masterView.noteType[self.masterView.selectedRow] = type[pickerLocation]
        print(category)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        categoryPicker.dataSource=self;
        categoryPicker.delegate=self;
        textView.text = text
        
        let borderColor = UIColor.black
        textView.layer.borderColor = borderColor.cgColor;
        textView.layer.borderWidth = 1.0;
        print(pickerLocation)
        categoryPicker.selectRow(pickerLocation, inComponent: 0, animated: true)
        
        // Do any additional setup after loading the view.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tap)

    }
    
    //Calls this function when the tap is recognized.
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func setNote(msg:String, category:String) {
        
        text = msg
        pickerLocation = 2;
        print("hereee")
        if isViewLoaded {
            print("and hereee")
            textView.text = text
           
            print(pickerLocation)
            
        }
       /* else {print("trying to load view?")
            self.viewDidLoad()}*/
        if category == "Grammar"{
            pickerLocation = 0;
        }
        if category == "Vocabulary"{
            pickerLocation = 1;
            
        }
        if category == "Phrase"{
            pickerLocation = 2;
            
        }
        if category == "Slang"{
            pickerLocation = 3;
            
        }
        
        
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //changing the title colour
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        //changing the tint colour
        self.navigationController?.navigationBar.tintColor = UIColor.white
        //changing the nav bar colour
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 131/255, green: 109/255, blue: 167/255, alpha: 1)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        message = textView.text
        masterView.comingFromDetail = true
        masterView.newRowText = message
        masterView.save()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
