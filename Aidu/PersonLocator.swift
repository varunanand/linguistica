//
//  Person.swift
//  MapsTestPoc
//
//  Created by Elara on 8/1/17.
//  Copyright © 2017 Elara. All rights reserved.
//

import Foundation


import MapKit
import UIKit

class PersonLocator: NSObject, MKAnnotation {
    var title: String?
    var coordinate: CLLocationCoordinate2D
    var personId: String?
    var isContact: Bool!
    var isNative: Bool!
    
    
    init(title: String, coordinate: CLLocationCoordinate2D, personId: String, isContact: Bool, isNative: Bool) {
        self.title = title
        self.coordinate = coordinate
        self.personId = personId
        self.isContact = isContact
        self.isNative = isNative
    }
}
