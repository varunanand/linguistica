//
//  BioViewController.swift
//  Aidu
//
//  Created by Elara on 2/14/18.
//  Copyright © 2018 Aidu. All rights reserved.
//

import UIKit

class BioViewController: UIViewController {
    var bio:String!
    
    @IBOutlet weak var bioField: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.bioField.text = self.bio
        
        // Gesture recogniser for keyboard to go away on tapping anywhere on page.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UserProfileViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }

    @IBAction func doneButton(_ sender: Any) {
        bio = bioField.text
      
        performSegue(withIdentifier: "doneBio", sender: self)
        
    }
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "doneBio" {
            
            
            let controller = segue.destination as! UserProfileViewController
            controller.editedBio = bio
            
            
        }
        
        
        
    }

}
