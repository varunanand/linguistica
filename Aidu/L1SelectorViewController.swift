//
//  L1SelectorViewController.swift
//  Aidu
//
//  Created by Elara on 2/11/18.
//  Copyright © 2018 Aidu. All rights reserved.
//

import UIKit

class L1SelectorViewController: UITableViewController {
    var selected = Int()
    let list = ["Mandarin", "English"]
    
    @IBOutlet var table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        table.reloadData()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selected = indexPath.row
        table.reloadData()
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "L1Cell")
        cell.textLabel?.text = list[indexPath.row]
        print("indexpath printing")
        print(indexPath.row)
        if(selected == indexPath.row){
            cell.accessoryType = .checkmark
        }
        else{
            cell.accessoryType = .none
        }
        
        return cell
    }
    
    
    override  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    @IBAction func doneButton(_ sender: Any) {
        performSegue(withIdentifier: "L1Completed", sender: self)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "L1Completed" {
            let controller = segue.destination as! UserProfileViewController
            if(self.selected == 0){
                
                print("sending controller back selected value is Mandarin")
                
                controller.editedL1 = "Mandarin"
                //controller.persistData()
            }
            else{
                
                print("sending controller back selected value is English")
                
                controller.editedL1 = "English"
                //controller.persistData()
            }
            
            
        }
    }
}
