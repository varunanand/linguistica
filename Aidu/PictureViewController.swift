//
//  PictureViewController.swift
//  Aidu
//
//  Created by varun murali on 10/07/17.
//  Copyright © 2017 Aidu. All rights reserved.
//

import UIKit

class PictureViewController: UIViewController {
    
    
    
    
    @IBOutlet weak var profilePic: UIImageView!
    var profile = ProfileData()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let data =  NSData(contentsOf: profile.picture!);
        print("the value of data is" , profile.picture);
        
        self.profilePic.image = UIImage(data:data! as Data)

        // Do any additional setup after loading the view.
    }
    @IBAction func Next(_ sender: Any) {
        performSegue(withIdentifier: "next", sender: self)
    }

   
    @IBAction func Back(_ sender: Any) {
        performSegue(withIdentifier: "back", sender: self)
    }
   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "next" {
            let controller = segue.destination as! LanguageViewController
            controller.profile = profile;
        }
        if segue.identifier == "back" {
            let controller = segue.destination as! InputNameViewController
            controller.profile = profile;
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
