//
//  Profile.swift
//  Aidu
//
//  Created by Elara on 11/24/17.
//  Copyright © 2017 Aidu. All rights reserved.
//

import Foundation
import Firebase

class Profile{
    
    class func downloadAllHobbies(forUser: String, completion: @escaping (String) -> Swift.Void)  {
    
    Database.database().reference().child("User").child(forUser).child("Hobby").observe(.childAdded, with: { (snapshot) in
        
        
       
        if snapshot.exists() {
            let data = snapshot.value as! String
            completion(data + "  |  " )
        }
      
        
        
    })
    
    
        
    }
    
    class func checkIfUserIsAContact(forUser: String, toUser: String, completion: @escaping (Bool) -> Swift.Void){
        var returnVal = false
        Database.database().reference().child("AiduUsers").child(forUser).observeSingleEvent(of: .value, with: { (snapshot) in
            
            if snapshot.exists() {
                //let enumeratorlist = snapshot.children
                let data = snapshot.value as! [String: Any]
                if(data["contacts"] != nil){
                    print("the data is",data["contacts"])
                var allContacts = data["contacts"] as! [String]
                for contact in allContacts
                {
                    if (toUser == contact){
                    returnVal = true;
                        break;}
                }
                completion(returnVal)
                }
                
            }
        })
        
    }
    
    class func downloadUserLangInfo(forUser: String, completion: @escaping (String,String) -> Swift.Void){
        Database.database().reference().child("User").child(forUser).observeSingleEvent(of: .value, with: { (snapshot) in
            
            if snapshot.exists() {
                let enumeratorlist = snapshot.children
                let data = snapshot.value as! [String: Any]
                completion(data["L1"]! as! String, data["L2"]! as! String)
               
                
            }
        })
        
    }
    
    class func downloadAllInterests(forUser: String, completion: @escaping (String) -> Swift.Void)  {
        
        Database.database().reference().child("User").child(forUser).child("intrestList").observe(.childAdded, with: { (snapshot) in
            
            
           
            if snapshot.exists() {
                let data = snapshot.value as! String
                completion(data + "  |  " )
            }
            
            //}
            
        })
    }
}
