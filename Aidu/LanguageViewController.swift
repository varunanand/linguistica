//
//  LanguageViewController.swift
//  Aidu
//
//  Created by varun murali on 12/07/17.
//  Copyright © 2017 Aidu. All rights reserved.
//

import UIKit

class LanguageViewController: UIViewController {
    var profile = ProfileData()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.nextButton.isHidden = true;
        // Do any additional setup after loading the view.
        if profile.L1 == "English"{
            self.English(self)
        }
        if profile.L1 == "Mandarin"{
            self.Mandrin(self)
        }
        //self.TestInput.attributedText = self.attributedText(first: "Are you ", second:"Frank Dolce", third: " ?")
        
    }
    
    //@IBOutlet weak var TestInput: UILabel!
    
    
    //to be removed --inputViewController
//    func attributedText (first:String, second:String, third:String)  -> NSAttributedString{
//        let string = first + second + third
//        let result = NSMutableAttributedString(string: string)
//  //      let attributesForSecondWord = [
//  //          NSFontAttributeName : UIFont.italicSystemFont(ofSize: 24).boldSystemFont(ofSize: 24)
////     ]
//        
//        result.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 24)  , range: NSMakeRange(first.characters.count, second.characters.count))
//        result.addAttribute(NSFontAttributeName, value: UIFont.italicSystemFont(ofSize: 24)  , range: NSMakeRange(first.characters.count , second.characters.count))
//        
//        return NSAttributedString(attributedString: result)
//    }
    
    @IBOutlet weak var nextButton: UIButton!
    
    
    
    @IBAction func Back(_ sender: Any) {
         performSegue(withIdentifier: "back", sender: self)
    }
    
    
    @IBOutlet weak var EnglishLang: UIButton!
    var b1White = true;
    var b2White = true;
    var onclicked = true;
    @IBAction func English(_ sender: Any) {
        profile.L1 = "English";
        
        b1White = !b1White;
        if(b1White == false && b2White == false){
            EnglishLang.backgroundColor = UIColor(red: 97.0 / 255.0, green: 61.0 / 255.0, blue: 156.0 / 255.0, alpha: 1.0);
            EnglishLang.titleLabel!.textColor = UIColor.white;
            MandarinLang.backgroundColor = UIColor.white
            profile.L1 = "English";
            b2White = true;
            b1White = false;
            EnglishLang.titleLabel!.textColor = UIColor.white;
            
            if(onclicked){
                self.EnglishLang.setTitleColor(UIColor.white, for: UIControlState.normal);
                self.MandarinLang.setTitleColor(UIColor.black, for: UIControlState.normal);
                self.nextButton.isHidden = false;
            }
            
            
        }
        else{
            if(b1White == true){
                EnglishLang.backgroundColor = UIColor.white;
                if(onclicked){
                    self.EnglishLang.setTitleColor(UIColor.black, for: UIControlState.normal);
                    self.nextButton.isHidden = true;
                }
                
            }
            else{
                EnglishLang.backgroundColor = UIColor(red: 97.0 / 255.0, green: 61.0 / 255.0, blue: 156.0 / 255.0, alpha: 1.0);
                profile.L1 = "English";
                if(onclicked){
                    self.EnglishLang.setTitleColor(UIColor.white, for: UIControlState.normal);
                    self.nextButton.isHidden = false;
                }
            }
            
        }
        
        
        
        
    }

    @IBOutlet weak var MandarinLang: UIButton!
    @IBAction func Mandrin(_ sender: Any) {
        profile.L1 = "Mandarin";
        
        self.nextButton.isHidden = false
        b2White = !b2White;
        if(b2White == false && b1White == false){
            MandarinLang.backgroundColor = UIColor(red: 97.0 / 255.0, green: 61.0 / 255.0, blue: 156.0 / 255.0, alpha: 1.0);
            MandarinLang.titleLabel!.textColor = UIColor.white;
            EnglishLang.backgroundColor = UIColor.white
            profile.L1 = "Mandarin";
            b2White = false;
            b1White = true;
            MandarinLang.titleLabel!.textColor = UIColor.white;
            if(onclicked){
                self.EnglishLang.setTitleColor(UIColor.black, for: UIControlState.normal);
                self.MandarinLang.setTitleColor(UIColor.white, for: UIControlState.normal);
                self.nextButton.isHidden = false;
            }
            
            
        }
        else{
            if(b2White == true){
                MandarinLang.backgroundColor = UIColor.white;
                if(onclicked){
                    self.MandarinLang.setTitleColor(UIColor.black, for: UIControlState.normal);
                    self.nextButton.isHidden = true;
                }
            }
            else{
                MandarinLang.backgroundColor = UIColor(red: 97.0 / 255.0, green: 61.0 / 255.0, blue: 156.0 / 255.0, alpha: 1.0);
                profile.L1 = "Mandarin";
                if(onclicked){
                    self.MandarinLang.setTitleColor(UIColor.white, for: UIControlState.normal);
                    self.nextButton.isHidden = false;
                }
            }
            
        }
        
        
        
        
    }
    
    

    @IBAction func next(_ sender: Any) {
            performSegue(withIdentifier: "next", sender: self)
        
        
    }
 
    
   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "next" {
            print("back is working");
            let controller = segue.destination as! LevelViewController
            
            controller.profile = profile;
        }
        if segue.identifier == "back" {
            print("back is working");
            let controller = segue.destination as! PictureViewController
            
            controller.profile = profile;
        }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
