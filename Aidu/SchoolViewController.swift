//
//  SchoolViewController.swift
//  Aidu
//
//  Created by varun murali on 12/07/17.
//  Copyright © 2017 Aidu. All rights reserved.
//

import UIKit

class SchoolViewController: UIViewController {
var profile = ProfileData()
    
    
@IBOutlet weak var School: UITextField!
    @IBAction func Next(_ sender: Any) {
       profile.school = School.text;
        performSegue(withIdentifier: "next", sender: self)
        
    }
    @IBAction func Back(_ sender: Any) {
        profile.school = School.text;
        performSegue(withIdentifier: "back", sender: self)
        
    }
  
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "next" {
            let controller = segue.destination as! CountryViewController
            controller.profile = profile;
        }
        if segue.identifier == "back" {
            let controller = segue.destination as! LevelViewController
            controller.profile = profile;
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        School.text = profile.school!
        // Do any additional setup after loading the view.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tap)
        
    }
    //Calls this function when the tap is recognized.
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
