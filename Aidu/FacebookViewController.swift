//
//  FacebookViewController.swift
//  Aidu
//
//  Created by varun murali on 10/07/17.
//  Copyright © 2017 Aidu. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FirebaseAuth
import FirebaseStorage


class FacebookViewController: UIViewController, FBSDKLoginButtonDelegate {
let loginManager = FBSDKLoginManager()
    var profile=ProfileData();
    
        func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
            
        print("User Logged out");
            loginManager.logOut()
            
    }
    
    ////my comment to check config management 
    /// comment for checkout of branch

var loginButton = FBSDKLoginButton()
    override func viewDidLoad() {
     
        // Comment to allow testing of onboarding
        super.viewDidLoad()
        if (Auth.auth().currentUser != nil) {
            //there is a user associated with the app on the phone, but we dont know from the db's perspective. So reauthenticate the user
            let user = Auth.auth().currentUser
            var credential: AuthCredential
            print("the value of AuthProvider")
            print(FacebookAuthProvider.description())
            
            
            if nil == FBSDKAccessToken.current(){
                //checking whether there is any FBSDKAccestoken
                //if no, let them sign in
                print("i am not logged in. second level if")
                
                self.loginButton.frame.origin = CGPoint(x: 37, y: 432)
                
                self.loginButton.readPermissions = ["public_profile", "email", "user_friends"]
                self.loginButton.delegate = self;
                self.view.addSubview(self.loginButton)
            }
            else{
            credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
            print("the value of credential")
            print(credential)
            user?.reauthenticate(with: credential) { error in
            if let error = error {
                //we tried to reauthenticate the user, but there was some error, hence login logic is being written
                print("i am not logged in. Innermost if")

                self.loginButton.frame.origin = CGPoint(x: 37, y: 432)

                self.loginButton.readPermissions = ["public_profile", "email", "user_friends"]
                self.loginButton.delegate = self;
                self.view.addSubview(self.loginButton)
                }
                else{
                //we have tried to reauthenticate the user and there is no error. We can successfully allow the user to go to home page
                // User re-authenticated.
                
                print("the value is not nil. Inner else. " , Auth.auth().currentUser?.uid)
                DispatchQueue.main.async(){
                        self.performSegue(withIdentifier: "home", sender: nil)
                }
        }
        
            }
        }
        }
        else{
            //Now this else is linked to the first if. There was no user found in the app. This is probably the first time that the user is loggin in. Implement the login logic in this case.
            print("i am not logged in. Outer else")
            
            self.loginButton.frame.origin = CGPoint(x: 37, y: 432)
            
            self.loginButton.readPermissions = ["public_profile", "email", "user_friends"]
            self.loginButton.delegate = self;
            self.view.addSubview(self.loginButton)
        }
        
        
        
        
        
        
 //working logic
//             let user = Auth.auth().currentUser
//              var credential: AuthCredential
//
//        // Prompt the user to re-provide their sign-in credentials
//            credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
//
//                user?.reauthenticate(with: credential) { error in
//                    if let error = error {
//
//                        print("i am not logged in")
//
//                        self.loginButton.frame.origin = CGPoint(x: 37, y: 432)
//
//                        self.loginButton.readPermissions = ["public_profile", "email", "user_friends"]
//                        self.loginButton.delegate = self;
//                        self.view.addSubview(self.loginButton)
//                    } else {
//                        // User re-authenticated.
//
//                        print("the value is not nil " , Auth.auth().currentUser?.uid)
//                        DispatchQueue.main.async(){
//                            self.performSegue(withIdentifier: "home", sender: nil)
//                        }
//                    }
//                }
//
//
//
//
//
//
//
        
//        print("the value is" ,Auth.auth().currentUser );
//        if (Auth.auth().currentUser != nil) {
//            print("the value is not nil " , Auth.auth().currentUser?.uid)
//            DispatchQueue.main.async(){
//            self.performSegue(withIdentifier: "home", sender: nil)
//            }
//        }
//        else{
//            print("i am not logged in")
//               // Optional: Place the button in the center of your view.
//        //button.frame.origin = CGPoint(x: 10, y: 10)
//        self.loginButton.frame.origin = CGPoint(x: 37, y: 432)
//        //self.loginButton.center = view.center
//        self.loginButton.readPermissions = ["public_profile", "email", "user_friends"]
//        self.loginButton.delegate = self;
//        view.addSubview(loginButton)
//        }
        
        

        // Do any additional setup after loading the view.
    }
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error?) {
        
        if let error = error {
            print(error.localizedDescription)
            return
        }
        else if result.isCancelled {
            print("you have not logged in")
            
        }
        else{
        let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
        Auth.auth().signIn(with: credential) { (user, error) in
            if let error = error {
                // ...
                print("There is an error",  error);
                return
            }
            else{
                
                let name=user!.displayName;
                let email=user!.email;
                let photoUrl=user!.photoURL;
                let uid=user!.uid;
                
                self.profile.name=name;
                self.profile.email=email;
                self.profile.picture=photoUrl;
                ProfileData.uid = uid;
                
                
               

                
                self.performSegue(withIdentifier: "SignedInSegue", sender: self)
                
              
                
            // User is signed in
            // I need to perform segue
            }
        }
        print("User Logged in Firebase");
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "SignedInSegue" {
            let controller = segue.destination as! FbLoggedInViewController
            controller.profile = profile;
        }
        if segue.identifier == "home" {
         print("i am going to map")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
