//
//  AcceptedFaceToFaceViewController.swift
//  Aidu
//
//  Created by varun murali on 28/11/17.
//  Copyright © 2017 Aidu. All rights reserved.
//

import UIKit

class AcceptedFaceToFaceViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func cancel(_ sender: Any) {
        self.performSegue(withIdentifier: "toChat", sender: nil)
    }
    @IBOutlet weak var cancel: UIButton!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
