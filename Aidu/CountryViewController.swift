//
//  CountryViewController.swift
//  Aidu
//
//  Created by varun murali/sriram jaikrishnan on 12/07/17.
//  Copyright © 2017 Aidu. All rights reserved.
//

import UIKit

class CountryViewController: UIViewController {
    
    // Autocomplete code snippet
    var autoCompleteViewController: AutoCompleteViewController!
    var statesList : [String] = []
    var citiesList : [String] = []
    var isFirstLoad: Bool = true
    
    // End
    var profile = ProfileData()
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if self.isFirstLoad {
            self.isFirstLoad = false
            Autocomplete.setupAutocompleteForViewcontroller(self)
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if profile.country == "USA"{
            self.USA(self)
        }
        if profile.country == "China"{
            self.China(self)
        }
        if profile.country == "Other"{
            self.Other(self)
        }
        
        StateText.text = profile.state
        
        CityText.text = profile.city
        
        
        // Do any additional setup after loading the view.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        tap.cancelsTouchesInView = false
        StateText.isUserInteractionEnabled = false
        CityText.isUserInteractionEnabled = false
        view.addGestureRecognizer(tap)
        
    }
    //Calls this function when the tap is recognized.
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    
    @IBOutlet weak var StateText: UITextField!
    @IBOutlet weak var CityText: UITextField!
    @IBOutlet weak var OtherCountry: UIButton!
    @IBOutlet weak var USACountry: UIButton!
    @IBOutlet weak var ChinaCountry: UIButton!
    var Chinapressed = true;
    var USApressed = true;
    var Otherpressed = true;
    @IBOutlet weak var State: UITextField!
    @IBOutlet weak var City: UITextField!
    
    @IBAction func China(_ sender: Any) {
        profile.country="China"
        if(Chinapressed){
            ChinaCountry.backgroundColor = UIColor(red: 97.0 / 255.0, green: 61.0 / 255.0, blue: 156.0 / 255.0, alpha: 1.0);
            OtherCountry.backgroundColor = UIColor.white
            self.statesList = china_states
            self.citiesList = china_city_names
            USACountry.backgroundColor = UIColor.white
            self.ChinaCountry.setTitleColor(UIColor.white, for: UIControlState.normal);
            self.USACountry.setTitleColor(UIColor.black, for: UIControlState.normal);
            self.OtherCountry.setTitleColor(UIColor.black, for: UIControlState.normal);
            profile.country="China";
            Chinapressed = false;
            Otherpressed = true;
            USApressed = true;
            
            let styledText1 = NSMutableAttributedString(string: "Sheng:")
            let styledText2 = NSMutableAttributedString(string: "Shi:")
            self.StateText.text = "";
            self.CityText.text = "";
            State.attributedText = styledText1;
            City.attributedText = styledText2;
            StateText.isUserInteractionEnabled = true
            CityText.isUserInteractionEnabled = true
            
        }
        else{
            ChinaCountry.backgroundColor = UIColor.white
            profile.country=nil;
            Chinapressed = true;
            // Otherpressed = false;
            // USApressed = false;
            self.ChinaCountry.setTitleColor(UIColor.black, for: UIControlState.normal);
            StateText.isUserInteractionEnabled = false
            CityText.isUserInteractionEnabled = false
            
        }
        
    }
    
    @IBAction func Other(_ sender: Any) {
        profile.country="Other"
        if(Otherpressed){
            OtherCountry.backgroundColor = UIColor(red: 97.0 / 255.0, green: 61.0 / 255.0, blue: 156.0 / 255.0, alpha: 1.0);
            USACountry.backgroundColor = UIColor.white
            ChinaCountry.backgroundColor = UIColor.white
            profile.country="Other";
            Otherpressed = false;
            Chinapressed = true;
            USApressed = true;
            
            self.ChinaCountry.setTitleColor(UIColor.black, for: UIControlState.normal);
            self.USACountry.setTitleColor(UIColor.black, for: UIControlState.normal);
            self.OtherCountry.setTitleColor(UIColor.white, for: UIControlState.normal);
            let styledText1 = NSMutableAttributedString(string: "State:")
            let styledText2 = NSMutableAttributedString(string: "City:")
            self.StateText.text = "";
            self.CityText.text = "";
            var statesList : [String] = []
            var citiesList : [String] = []
            State.attributedText = styledText1;
            City.attributedText = styledText2;
            StateText.isUserInteractionEnabled = true
            CityText.isUserInteractionEnabled = true
            
            
        }
        else{
            OtherCountry.backgroundColor = UIColor.white
            profile.country=nil;
            Otherpressed = true;
            // Chinapressed = false;
            // USApressed = false;
            self.OtherCountry.setTitleColor(UIColor.black, for: UIControlState.normal);
            StateText.isUserInteractionEnabled = false
            CityText.isUserInteractionEnabled = false
            
        }
    }
    
    
    @IBAction func USA(_ sender: Any) {
        profile.country="USA"
        if(USApressed){
            USACountry.backgroundColor = UIColor(red: 97.0 / 255.0, green: 61.0 / 255.0, blue: 156.0 / 255.0, alpha: 1.0);
            OtherCountry.backgroundColor = UIColor.white
            ChinaCountry.backgroundColor = UIColor.white
            profile.country="USA";
            USApressed = false;
            Chinapressed = true;
            Otherpressed = true;
            self.statesList = usa_states
            self.citiesList = usa_city_names
            self.ChinaCountry.setTitleColor(UIColor.black, for: UIControlState.normal);
            self.USACountry.setTitleColor(UIColor.white, for: UIControlState.normal);
            self.OtherCountry.setTitleColor(UIColor.black, for: UIControlState.normal);
            let styledText1 = NSMutableAttributedString(string: "State:")
            let styledText2 = NSMutableAttributedString(string: "City:")
            self.StateText.text = "";
            self.CityText.text = "";
            State.attributedText = styledText1;
            City.attributedText = styledText2;
            StateText.isUserInteractionEnabled = true
            CityText.isUserInteractionEnabled = true
            
            
        }
        else{
            USACountry.backgroundColor = UIColor.white
            profile.country=nil;
            USApressed = true;
            //  Chinapressed = false;
            //  Otherpressed = false;
            
            self.USACountry.setTitleColor(UIColor.black, for: UIControlState.normal);
            
            StateText.isUserInteractionEnabled = false
            CityText.isUserInteractionEnabled = false
            
        }
        
        
    }
    @IBAction func Next(_ sender: Any) {
        if(StateText.text != nil){
            profile.state = StateText.text;
        }
        if(CityText.text != nil){
            profile.city = CityText.text;
        }
        performSegue(withIdentifier: "next", sender: self)
        
        
        
    }
    
    @IBAction func Back(_ sender: Any) {
        performSegue(withIdentifier: "back", sender: self)
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "next" {
            let controller = segue.destination as! HobbyViewController
            controller.profile = profile;
        }
        if segue.identifier == "back" {
            let controller = segue.destination as! SchoolViewController
            controller.profile = profile;
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    
}
extension CountryViewController: AutocompleteDelegate {
    func autoCompleteTextFieldForState() -> UITextField {
        return self.StateText
    }
    func autoCompleteTextFieldForCity() -> UITextField {
        return self.CityText
    }
    func autoCompleteThreshold(_ textField: UITextField) -> Int {
        return 1
    }
    
    func autoCompleteItemsForSearchTermForState(_ term: String) -> [AutocompletableOption] {
        let filteredStates = self.statesList.filter { (states) -> Bool in
            return states.lowercased().contains(term.lowercased())
        }
        
        let States: [AutocompletableOption] = filteredStates.map { ( state) -> AutocompleteCellData in
            var state = state
            state.replaceSubrange(state.startIndex...state.startIndex, with: String(state.characters[state.startIndex]).capitalized)
            return AutocompleteCellData(text: state)
            }.map( { $0 as AutocompletableOption })
        
        if !Otherpressed{
        return []
        }
        else{
            return States
        }
    }
    
    func autoCompleteItemsForSearchTermForCity(_ term: String) -> [AutocompletableOption] {
        let filteredCities = self.citiesList.filter { (cities) -> Bool in
            return cities.lowercased().contains(term.lowercased())
        }
        
        let Cities: [AutocompletableOption] = filteredCities.map { ( city) -> AutocompleteCellData in
            var city = city
            city.replaceSubrange(city.startIndex...city.startIndex, with: String(city.characters[city.startIndex]).capitalized)
            return AutocompleteCellData(text: city)
            }.map( { $0 as AutocompletableOption })
        
        if !Otherpressed{
            return []
        }
        else{
            return Cities
        }
       
    }
    
    func autoCompleteHeight() -> CGFloat {
        return self.view.frame.height / 3.0
    }
    
    
    func didSelectItem(_ item: AutocompletableOption) {
        //print("Hi")
        //self.StateText.text = item.text
    }
    
    
}


