//
//  FacebookIconViewController.swift
//  Aidu
//
//  Created by varun murali on 06/08/17.
//  Copyright © 2017 Aidu. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FirebaseAuth
import FirebaseStorage

class FacebookIconViewController: UIViewController ,FBSDKLoginButtonDelegate {
    
    let loginManager = FBSDKLoginManager()
    var profile=ProfileData();
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        
        print("User Logged out");
        loginManager.logOut()
        
    }
    
    ////my comment to check config management
    /// comment for checkout of branch
    
    var loginButton = FBSDKLoginButton()


    override func viewDidLoad() {
        super.viewDidLoad()
        // Optional: Place the button in the center of your view.
        self.loginButton.center = view.center
        self.loginButton.readPermissions = ["public_profile", "email", "user_friends"]
        self.loginButton.delegate = self;
        view.addSubview(loginButton)
        

        

        // Do any additional setup after loading the view.
    }
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error?) {
        
        if let error = error {
            print(error.localizedDescription)
            return
        }
        else if result.isCancelled {
            print("you have not logged in")
            
        }
        else{
            let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
            Auth.auth().signIn(with: credential) { (user, error) in
                if let error = error {
                    // ...
                    print("There is an error",  error);
                    return
                }
                else{
                    
                    let name=user!.displayName;
                    let email=user!.email;
                    let photoUrl=user!.photoURL;
                    let uid=user!.uid;
                    
                    self.profile.name=name;
                    self.profile.email=email;
                    self.profile.picture=photoUrl;
                    ProfileData.uid = uid;
                    
                    
                    
                    
                    
                    self.performSegue(withIdentifier: "next", sender: self)
                    
                    
                    
                    // User is signed in
                    // I need to perform segue
                }
            }
            print("User Logged in Firebase");
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "next" {
            let controller = segue.destination as! FbLoggedInViewController
            controller.profile = profile;
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
