//
//  ContactsReader.swift
//  MapsTestPoc
//
//  Created by Elara on 8/1/17.
//  Copyright © 2017 Elara. All rights reserved.
//

import Foundation
import Firebase
class Contact{
    var contactId: String?
    
}

class ContactsReaderForMap: NSObject{
    static let sharedContacts = ContactsReaderForMap()
    var contactsList: [Contact?] = []
    static var userId : String?;
    
    
    
    func getContacts(currentlyLoggedInUser: String!) {
        ContactsReaderForMap.userId = currentlyLoggedInUser;
        
        Database.database().reference().child("AiduUsers").child(currentlyLoggedInUser).child("contacts").observe(.childAdded, with: {(snapshot) in
            let contactDict =  snapshot.value as! [String: AnyObject]
            
            var contact = Contact()
            contact.contactId = snapshot.key;
            
            //print("Contacts are",contact.contactId,contact.contactName);
            self.contactsList.append(contact)
            
            
            
            
            
        }, withCancel: nil)
    
}
}
