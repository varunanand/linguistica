//
//  L2SelectorViewController.swift
//  Aidu
//
//  Created by Elara on 2/10/18.
//  Copyright © 2018 Aidu. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseMessaging
import Firebase;

class L2SelectorViewController: UITableViewController{
    var selected = Int()
    let list = ["Mandarin", "English"]
  
    @IBOutlet var table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        table.reloadData()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
            selected = indexPath.row
            table.reloadData()
        
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "L2Cell")
        cell.textLabel?.text = list[indexPath.row]
        print("indexpath printing")
        print(indexPath.row)
        if(selected == indexPath.row){
            cell.accessoryType = .checkmark
        }
        else{
            cell.accessoryType = .none
        }
        
        return cell
    }


   override  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 2
    }

    @IBAction func doneButton(_ sender: Any) {
        performSegue(withIdentifier: "L2Completed", sender: self)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "L2Completed" {
            let controller = segue.destination as! UserProfileViewController
            if(self.selected == 0){
                
                print("sending controller back selected value is Mandarin")
                
                controller.editedL2 = "Mandarin"
                //controller.persistData()
            }
            else{
               
                print("sending controller back selected value is English")
                
                controller.editedL2 = "English"
                //controller.persistData()
            }
        
            
        }
    }
}
