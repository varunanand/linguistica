//
//  ProfileCardViewController.swift
//  Aidu
//
//  Created by Aidu on 7/27/17.
//  Copyright © 2017 Aidu. All rights reserved.
//

import UIKit
import FirebaseDatabase

class ProfileCardViewController: UIViewController {
    

    @IBOutlet weak var nameLabel: UILabel!
    
    var ref: DatabaseReference!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        
        ref = Database.database().reference()

        // only need to fetch once so use single event
        
        ref.child("Aidu").child("l0nWm4Odv9Y60z9zlaEEhUA0KsF2").observeSingleEvent(of: .value, with: { snapshot in
            
            if !snapshot.exists() {
                
                print("No snapshot")
            }
            
            //print(snapshot)
            if let value = snapshot.value as? [String: AnyObject] {
            let username = value["username"] as? String ?? ""
            let email = value["email"] as? String ?? ""
            let L1 = value["L1"] as? String ?? ""
            let L2 = value["L2"] as? String ?? ""
            let level = value["level"] as? String ?? ""
            let school = value["school"] as? String ?? ""
            let country = value["county"] as? String ?? ""
            let state = value["state"] as? String ?? ""
            let city = value["city"] as? String ?? ""
            //let intrest = value["intrest"] as? NSDictionary
            //let intrestList = value["intrestList"] as? NSDictionary
            let ownStory = value["ownStory"] as? String ?? ""
            print(username + " " + email + " " + L1 + " " + L2 + " " + level + " " + school + " " + country + " " + state + " " + city + " "  + ownStory)
            self.nameLabel.text = username
            //print(intrest)
            //print("This is the console output: \(intrestList as AnyObject)")
            
        
            }
            })
            
            // can also use
            // snapshot.childSnapshotForPath("full_name").value as! String

    }
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
