//
//  PlayAudioVC.swift
//  Voice Recorder
//
//  Created by Elara on 10/29/17.
//  Copyright © 2017 Elara. All rights reserved.
//

import UIKit
import AVFoundation
import Firebase
class ContactsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {
    //var audioPlayer:AVAudioPlayer!
    //var dirPath:String! // Path to recorded files.
    //var fileNames = [String]() // File names of all saved recordings.
    let textCellIdentifier = "TextCell"
    var itemsFriends = [User]()
    var itemsPendingRequests = [User]()
    var window: UIWindow?
    var currentSelectedUser: String = ""
    var usersToBeSent = [UsersOnTheMapView]()
    
    @IBOutlet weak var requestsSegmentedControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
   
    
    @IBAction func requestsChanged(_ sender: Any) {
        self.tableView.reloadData()
    }
    func fetchContacts()  {
        if let id = Auth.auth().currentUser?.uid {
            User.downloadAllContacts(forUser: id, completion: {(user) in
                DispatchQueue.main.async {
                    self.itemsFriends.append(user)
                    //self.tableView.delegate=self;
                    //self.tableView.dataSource=self
                    //print(self.items.count)
                    self.itemsFriends.sort { $0.name < $1.name }
                    self.tableView.reloadData()
                    //self.collectionView.reloadData()
                }
            })
            User.downloadAllPendingRequests(forUser: id, completion: {(user) in
                DispatchQueue.main.async {
                    self.itemsPendingRequests.append(user)
                    print("Printing user lets see what comes up")
                  print(user.id)
                    self.itemsPendingRequests.sort { $0.name < $1.name }
                    self.tableView.reloadData()
                    
                }
            })
       
        }}
    override func viewDidLoad()
    {
        super.viewDidLoad()
        fetchContacts()
        

            
        }
        
        
    
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // This is only regarding the tableView
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        switch requestsSegmentedControl.selectedSegmentIndex {
        case 0:
            return self.itemsFriends.count
        case 1:
            return self.itemsPendingRequests.count
        
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: textCellIdentifier, for: indexPath as IndexPath) as! ContactListTBCell
        //let row = indexPath.row
        switch requestsSegmentedControl.selectedSegmentIndex {
        case 0:
            cell.nameLabel.text = self.itemsFriends[indexPath.row].name
            cell.profilePic.image = self.itemsFriends[indexPath.row].profilePic
        case 1:
            cell.nameLabel.text = self.itemsPendingRequests[indexPath.row].name
            cell.profilePic.image = self.itemsPendingRequests[indexPath.row].profilePic
        default:
            return cell
        
        
        }
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "contactToProfile" {
            let controller = segue.destination as! ProfilePageViewController
            controller.users = self.usersToBeSent;
            controller.currentUser = self.currentSelectedUser;
        }
        
    }
    
    // MARK: UITableViewDelegate Methods
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
        
        let row = indexPath.row
        switch requestsSegmentedControl.selectedSegmentIndex {

        case 1:
           //write logic to go to profile page
            print("for now")
            print(itemsPendingRequests[row].id)
            currentSelectedUser = itemsPendingRequests[row].id
            for object in itemsPendingRequests{
                let a = UsersOnTheMapView();
                
                a.userId = object.id
                a.userName = object.name
                usersToBeSent.append(a)
            }
            //nirav addition
            self.performSegue(withIdentifier: "contactToProfile", sender: self)
            //nirav
        default:
            print("nothing")
            
            
        }
        
    }
}
