//
//  ViewController.swift
//  Plain Ol' Notes
//
//  Created by Todd Perkins on 12/6/16.
//  Copyright © 2016 Todd Perkins. All rights reserved.
//

import UIKit
import AVFoundation

class NotesViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var table: UITableView!
    
@IBOutlet weak var NotesAudioTab: UISegmentedControl!
    
    var audioPlayer:AVAudioPlayer!
    var dirPath:String! // Path to recorded files.
    var fileNames = [String]() // File names of all saved recordings.
    let textCellIdentifier = "TextCell"
    var noteMessages:[String] = []
    var noteTimeStamps:[String] = []
    var noteType:[String] = []
    var recordType:[String] = []
    var selectedRow:Int = -1
    var newRowText:String = ""
    var comingFromDetail:Bool = false
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    @IBAction func TabValueChanged(_ sender: Any) {
        //self.viewDidLoad()
       table.reloadData()
        switch(NotesAudioTab.selectedSegmentIndex){
        case 0:
            let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addNote))
            addButton.tintColor = UIColor(red: 131/255, green: 109/255, blue: 167/255, alpha: 1)
            
            self.navigationItem.rightBarButtonItem = addButton
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            
            print("enabling add button")
            break
        case 1:
            let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addAudio))
            addButton.tintColor = UIColor(red: 131/255, green: 109/255, blue: 167/255, alpha: 1)
            
            self.navigationItem.rightBarButtonItem = addButton
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            
            print("disabling add button")
            break
        default:
            print("default add button")
            let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addNote))
            addButton.tintColor = UIColor(red: 131/255, green: 109/255, blue: 167/255, alpha: 1)
            
            self.navigationItem.rightBarButtonItem = addButton
            break
        }
    }

    override func viewDidLoad() {
        print("record type size ")
        print(recordType.count)
        super.viewDidLoad()
        print("i am checking if i came to view did load")
        load()
        
        // Do any additional setup after loading the view, typically from a nib.
        
        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addNote))
        switch(NotesAudioTab.selectedSegmentIndex){
        case 1:
            let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addAudio))
       
            break
        default: let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addNote))
       
            break
        }
        
        //self.navigationController?.navigationBar.barTintColor = UIColor(red: 131/255, green: 109/255, blue: 167/255, alpha: 1)
        addButton.tintColor = UIColor(red: 131/255, green: 109/255, blue: 167/255, alpha: 1)
        
        self.navigationItem.rightBarButtonItem = addButton
        self.navigationItem.leftBarButtonItem = editButtonItem
        editButtonItem.tintColor = UIColor(red: 131/255, green: 109/255, blue: 167/255, alpha: 1)
        self.navigationItem.title = "Notes"
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        if selectedRow == -1 {
            return
        }
        load()
        print("loaded data")
        print("viewwillappear")
        print(noteMessages)
        switch(NotesAudioTab.selectedSegmentIndex){
        case 0:
            print("viewwillappear 0")
         //    data[selectedRow] = newRowText
            if comingFromDetail{
            noteMessages[selectedRow] = newRowText
            }
            //else{ noteMessages[selectedRow] = noteMessages[selectedRow] }
            comingFromDetail = false
                        if newRowText == "" {
                            noteMessages.remove(at: selectedRow)
                            noteTimeStamps.remove(at: selectedRow)
                            noteType.remove(at: selectedRow)
                        }
            
            table.reloadData()
            print("sdkjsndfhbsfousybdf")
            save()
            break
        case 1:
            print("viewwillappear 1")
            table.reloadData()
            break
        default:
            print("viewwillappear This is the default value")
            
            break
        }
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("the view did appeare");
        
        self.navigationController?.navigationBar.barTintColor = UIColor.white
       
        print("viewdidappear")
        switch(NotesAudioTab.selectedSegmentIndex){
        case 0:
            print("viewdidappear 0")
            load()
            table.reloadData()
            print("sdkjsndfhbsfousybdf")
            save()
            break
        case 1:
            print("viewdidappear 1")
            load()
            table.reloadData()
            break
        default:
            print("viewdidappear This is the default value")
            
            break
        }
        

    }
    
    func addAudio(){
        self.performSegue(withIdentifier: "recordScreen", sender: nil)
    }
    
    func addNote() {
        if (table.isEditing) {
            return
        }
        
        selectedRow = 0
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
     //   let someDateTime = formatter.date(from: "2016/10/08 22:31")
        let currentDateTime = Date()
        let someDateTime = formatter.string(from: currentDateTime)
        
        let name:String = "initialnote"
        let ts:String = someDateTime

        let type:String = "Grammar"
        
        noteMessages.insert(name, at: 0)
        noteTimeStamps.insert(ts, at: 0)
        noteType.insert(type, at: 0)
       // data.insert(name, at: 0)
        
        let indexPath:IndexPath = IndexPath(row: 0, section: 0)
        table.insertRows(at: [indexPath], with: .automatic)
        table.selectRow(at: indexPath, animated: true, scrollPosition: .none)
        self.performSegue(withIdentifier: "detail", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return data.count
        var returnValue = 0
        switch(NotesAudioTab.selectedSegmentIndex){
        case 0:
            print("countRowshello 0")
            returnValue = noteMessages.count
            print(returnValue)
            break
        case 1:
            print("countRowshello 1")
            returnValue = fileNames.count
            print(returnValue)
            break
        default:
                print("countRowshello This is the default value")
                print(returnValue)
                returnValue = 0;
            break
        }
       // print("the return value")
        
        return returnValue
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "note",  for: indexPath) as! NoteCell
        switch(NotesAudioTab.selectedSegmentIndex){
        case 0:
            
            cell.title.text = noteMessages[indexPath.row]
            cell.timeStamp.text = noteTimeStamps[indexPath.row]
            cell.category.text = noteType[indexPath.row]
            print("cellForRowAthello 0")
           // return cell1
           break
        case 1:
            //cell.textLabel?.text = data[indexPath.row]
         // let cell = tableView.dequeueReusableCell(withIdentifier: "note", for: indexPath as IndexPath)
            let row = indexPath.row
            cell.title.text = fileNames[row]
            print("recordType count incellsfor")
            print(recordType.count)
            cell.category.text = recordType[row]
            cell.timeStamp.text = " "
           // cell.textLabel?.text = fileNames[row]
            
            print("cellForRowAthello 1")
           // return cell2
            break
        default:
            print("cellForRowAt This is the default value")
            
            break
        }
        print("the return value")
       
       
    
       return cell
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        
        switch(NotesAudioTab.selectedSegmentIndex){
        case 0:
            print("setEditinghello 0")
            super.setEditing(editing, animated: animated)
            table.setEditing(editing, animated: animated)
            break
        case 1:
            print("setEditinghello 1")
            super.setEditing(editing, animated: animated)
            table.setEditing(editing, animated: animated)
            break
        default:
            print("setEditingThis is the default value")
            
            break
        }
        
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {

        switch(NotesAudioTab.selectedSegmentIndex){
        case 0:
            print("editingStylehello 0")
            noteMessages.remove(at: indexPath.row)
            noteTimeStamps.remove(at: indexPath.row)
            noteType.remove(at: indexPath.row)
            table.deleteRows(at: [indexPath], with: .fade)
            save()
            break
        case 1:
            print("editingStylehello 1")
            //returnValue = fileNames.count
            print(selectedRow)
            //selectedRow = 0
            let fileToDelete = String(NSSearchPathForDirectoriesInDomains(.documentDirectory,.userDomainMask,true)[0]) + "/" + String(fileNames[indexPath.row])
            print("Step 1 ")
            let url=NSURL.fileURL(withPath: fileToDelete)
            
            print("Step 2 ")
            do {
                print("Step 3 ")
                try FileManager.default.removeItem(at: url)
                print("Step 4 ")
            } catch let error as NSError {
                print("Error: \(error.domain)")
            }
            print("Step 5 ")
            fileNames.remove(at: indexPath.row)
            print("Step 6 ")
            recordType.remove(at: indexPath.row)
            print("Step 7 ")
            table.deleteRows(at: [indexPath], with: .fade)
            //table.reloadData()
            save()
            break
        default:
            print("editingStyleThis is the default value")
            //returnValue = 0;
            break
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        switch(NotesAudioTab.selectedSegmentIndex){
        case 0:
            print("didSelectRowAthello 0")
            self.performSegue(withIdentifier: "detail", sender: nil)
            break
        case 1:
            print("didSelectRowAthello 1")
            
            self.performSegue(withIdentifier: "play", sender: nil)
            table.deselectRow(at: indexPath as IndexPath, animated: true)
            
            let row = indexPath.row
            
            
            break
        default:
            print("didSelectRowAtThis is the default value")
           // returnValue = 0;
            break
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "detail"{
        let detailView:NotesDetailViewController = segue.destination as! NotesDetailViewController
        
            selectedRow = table.indexPathForSelectedRow!.row
        
        detailView.masterView = self
        detailView.setNote(msg: noteMessages[selectedRow], category: noteType[selectedRow])
        }
        else if segue.identifier == "play"{
            let detailView:PlayDetailViewController = segue.destination as! PlayDetailViewController
            selectedRow = table.indexPathForSelectedRow!.row
            detailView.masterView = self
            detailView.setPlay(msg: fileNames[selectedRow], category: recordType[selectedRow])
        }
        
    }
    
    
    
    func save() {
      //  UserDefaults.standard.set(data, forKey: "notes")
     //  UserDefaults.standard.synchronize()
        print("came to save")
        UserDefaults.standard.set(noteMessages, forKey: "notes")
        UserDefaults.standard.set(noteTimeStamps, forKey: "noteTimeStamps")
        UserDefaults.standard.set(noteType, forKey: "noteTypes")
        UserDefaults.standard.set(recordType, forKey: "recordTypes")
         print("should come here ")
        UserDefaults.standard.synchronize()
    }
    
    func load() {
        if let loadedData = UserDefaults.standard.value(forKey: "notes") as? [String] {
//            data = loadedData
//            table.reloadData()
            noteMessages = loadedData
            
        }
        if let loadedts = UserDefaults.standard.value(forKey: "noteTimeStamps") as? [String] {
            //            data = loadedData
            //            table.reloadData()
            noteTimeStamps = loadedts
            
        }
        if let loadedtype = UserDefaults.standard.value(forKey: "noteTypes") as? [String] {
            //            data = loadedData
            //            table.reloadData()
            noteType = loadedtype
            
        }
        
        if let loadedRecordTypes = UserDefaults.standard.value(forKey: "recordTypes") as? [String] {
            //            data = loadedData
            //            table.reloadData()
            recordType = loadedRecordTypes
            print("record type oda size")
            print(recordType.count)
        }
        
        dirPath = NSSearchPathForDirectoriesInDomains(.documentDirectory,.userDomainMask,true)[0]
        
        // Find all m4a-files
        let filemanager:FileManager = FileManager()
        let files = filemanager.enumerator(atPath: dirPath)
        let mp3Files = files!.filter{($0 as AnyObject).pathExtension == "m4a"}
        //print("mp3 urls:",mp3Files)
        
        for mp3File in mp3Files{
        
            if !fileNames.contains(mp3File as! String){
                fileNames.append (mp3File as! String)
            }
            
            
            if recordType.count < fileNames.count {
                print("recordType count")
                print(recordType.count)
                print("filename count")
                print(fileNames.count)
                recordType.append("Grammar")
                save()
                print("recordType count")
                print(recordType.count)
                
            }
        }
        table.reloadData()
        print("note messages prinitng")
        print(noteMessages)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

