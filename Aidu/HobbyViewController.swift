//
//  HobbyViewController.swift
//  Aidu
//
//  Created by varun murali on 12/07/17.
//  Copyright © 2017 Aidu. All rights reserved.
//

import UIKit

class HobbyViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var profile = ProfileData()
    private var hobbyList = Hobby.getMockData()
    
    @IBAction func Next(_ sender: Any) {
        performSegue(withIdentifier: "next", sender: self)
        
    }
    
    @IBAction func back(_ sender: Any) {
        performSegue(withIdentifier: "back", sender: self)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "next" {
            let controller = segue.destination as! IntrestViewController
            controller.profile = profile;
        }
        
        if segue.identifier == "back" {
            let controller = segue.destination as! CountryViewController
            controller.profile = profile;
        }
    }
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //ArtButton.backgroundColor = UIColor.gray
        // Do any additional setup after loading the view.
        
    }
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return hobbyList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Hobby", for: indexPath)
        
        if indexPath.row < hobbyList.count
        {
            let item = hobbyList[indexPath.row]
            cell.textLabel?.text = item.title
            if profile.intrest.contains(item.title){
                item.isIntrested = true

            }
            let accessory: UITableViewCellAccessoryType = item.isIntrested ? .checkmark : .none
            cell.accessoryType = accessory
            
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row < hobbyList.count
        {
            let item = hobbyList[indexPath.row]
            item.isIntrested = !item.isIntrested;
            if profile.intrest.contains(item.title){
                //let str = item.title as String!
                print(item.title)
              let ind =  profile.intrest.index(of: item.title as String!) as Int!
                profile.intrest.remove(at: ind!)
            }
            else {profile.intrest.append(item.title);}
            print(profile.intrest);
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
