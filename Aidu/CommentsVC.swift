
//  MIT License

//  Copyright (c) 2017 Haik Aslanyan

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.


import UIKit
import Photos
import Firebase
import CoreLocation

class CommentsVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate,  UINavigationControllerDelegate, UIImagePickerControllerDelegate, CLLocationManagerDelegate {
    
    //MARK: Properties
    
    @IBOutlet var inputBar: UIView!
    
    
    @IBOutlet weak var tableView: UITableView!
    
    
    @IBOutlet weak var inputTextField: UITextField!
    
    @IBOutlet weak var praiseImage: UIImageView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    var longPressMessageRowSelected: Int!
    var longPressMessageBoolFlag: Bool!
    var messageReceived: Message!
    var messageAsNote: String!
    var noteMessages:[String] = []
    var noteTimeStamps:[String] = []
    var noteType:[String] = []
  //  var messageReceivedFrom: MessageOwner!
    
    @IBAction func commentMessage(_ sender: Any) {
        if let text = self.inputTextField.text {
            if text.characters.count > 0 {
                self.composeMessage(type: .text, content: self.inputTextField.text!)
                self.inputTextField.text = ""
                
            }
        }
    }
    
    override var inputAccessoryView: UIView? {
        get {
            self.inputBar.frame.size.height = self.barHeight
            self.inputBar.clipsToBounds = true
            return self.inputBar
        }
    }
    override var canBecomeFirstResponder: Bool{
        return true
    }
    let locationManager = CLLocationManager()
    var items = [Comment]()
    //var temp: Message?;
    let imagePicker = UIImagePickerController()
    let barHeight: CGFloat = 50
    var currentUser: User?
    var canSendLocation = true
    
    
    //MARK: Methods
    func customization() {
        self.imagePicker.delegate = self
        self.tableView.estimatedRowHeight = self.barHeight
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.contentInset.bottom = self.barHeight
        self.tableView.scrollIndicatorInsets.bottom = self.barHeight
        self.navigationItem.title = self.currentUser?.name
        let icons = UIImage.init(named: "back")?.withRenderingMode(.alwaysOriginal)
        let backButton = UIBarButtonItem.init(image: icons!, style: .plain, target: self, action: #selector(self.dismissSelf))
        self.navigationItem.leftBarButtonItem = backButton
        let icon = UIImage(named: "save")
        let iconSize = CGRect(origin: CGPoint.zero, size: CGSize(width: 80, height: 13))
        let iconButton = UIButton(frame: iconSize)
        iconButton.setBackgroundImage(icon, for: .normal)
        let barButton = UIBarButtonItem(customView: iconButton)
        self.navigationItem.rightBarButtonItem = barButton
        iconButton.addTarget(self, action: #selector(handleSaveToNotes), for: .touchUpInside)
        self.locationManager.delegate = self
    }
    
    func handleSaveToNotes(){
       // print("coming to handleSaveToNotes")
       // print(messageAsNote)

       addNote()
        
        let alert = UIAlertController(title: "Saved to Notes", message: "The message along with all its comments was successfully stored to Notes.", preferredStyle: UIAlertControllerStyle.alert)
        self.present(alert, animated: true, completion: nil)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
    }
    
    
    func addNote() {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        
        let currentDateTime = Date()
        let someDateTime = formatter.string(from: currentDateTime)
        
        let name:String = messageAsNote
        let ts:String = someDateTime
        
        let type:String = "Grammar"
        
        let sb = UIStoryboard(name: "Main", bundle: nil)

       let otherVC = sb.instantiateViewController(withIdentifier: "noteController") as! NotesViewController
        //otherVC.load()
        
        if let loadedData = UserDefaults.standard.value(forKey: "notes") as? [String] {
          
            noteMessages = loadedData

        }
        if let loadedts = UserDefaults.standard.value(forKey: "noteTimeStamps") as? [String] {
            
            noteTimeStamps = loadedts

        }
        if let loadedtype = UserDefaults.standard.value(forKey: "noteTypes") as? [String] {
          
            noteType = loadedtype

        }
        //print(noteMessages)
        
        noteMessages.insert(name, at: 0)
        noteTimeStamps.insert(ts, at: 0)
        noteType.insert(type, at: 0)
        
        //print(noteMessages)
        UserDefaults.standard.set(noteMessages, forKey: "notes")
        UserDefaults.standard.set(noteTimeStamps, forKey: "noteTimeStamps")
        UserDefaults.standard.set(noteType, forKey: "noteTypes")
       
        let indexPath:IndexPath = IndexPath(row: 0, section: 0)
      
         //print("came here in the function")
      
      
       
    }
    
  
    //Downloads messages
    func fetchData() {
        if(messageReceived.owner == .receiver){
            self.inputBar.isHidden = true
        }
        else{
            self.inputBar.isHidden = false
        }
        var c = Comment(messageId: messageReceived.messageId, fromID: messageReceived.fromID!, toID: messageReceived.toID!, type: messageReceived.type, content: messageReceived.content, owner: .sender, timestamp: messageReceived.timestamp, isRead: messageReceived.isRead, isPraised: messageReceived.isPraised)
        
        items.append((c))
        
        for element in self.messageReceived.comments
        {
            if(element != "Test"){
                var c = Comment(messageId: messageReceived.messageId, fromID: messageReceived.fromID!, toID: messageReceived.toID!, type: messageReceived.type, content: element, owner: .receiver, timestamp: messageReceived.timestamp, isRead: messageReceived.isRead, isPraised: messageReceived.isPraised)
                items.append((c))
            }
            
        }
        tableView.reloadData()
    }
    
    //Hides current viewcontroller
    func dismissSelf() {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    func composeMessage(type: MessageType, content: String)  {
        
        /*let message = Message.init(messageId: nil, fromID: "", toID: "", type: type, content: content, owner: .sender, timestamp: Int(Date().timeIntervalSince1970), isRead: false, isPraised: false, comments: ["Test"])
         Message.send(message: message, toID: self.currentUser!.id, completion: {(_) in
         })*/
        var fullCommentSet = self.messageReceived.comments;
        fullCommentSet.append(content)
        Message.commentMessage(comments: fullCommentSet, fromID: self.messageReceived.fromID! , toID: self.messageReceived.toID!, messageID: self.messageReceived.messageId!, completion: { _ in
            //items.a
            let w = Comment(messageId: self.messageReceived.messageId, fromID: self.messageReceived.fromID!, toID: self.messageReceived.toID!, type: self.messageReceived.type, content: content, owner: .receiver, timestamp: self.messageReceived.timestamp, isRead: self.messageReceived.isRead, isPraised: self.messageReceived.isPraised)
            self.items.append((w))
            self.tableView.reloadData()
            
            
        })
        
    }
    
    func checkLocationPermission() -> Bool {
        var state = false
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            state = true
        case .authorizedAlways:
            state = true
        default: break
        }
        return state
    }
    
    func animateExtraButtons(toHide: Bool)  {
        switch toHide {
        case true:
            self.bottomConstraint.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.inputBar.layoutIfNeeded()
            }
        default:
            self.bottomConstraint.constant = -50
            UIView.animate(withDuration: 0.3) {
                self.inputBar.layoutIfNeeded()
            }
        }
    }
    
    @IBAction func showMessage(_ sender: Any) {
        self.animateExtraButtons(toHide: true)
    }
    
    @IBAction func selectGallery(_ sender: Any) {
        self.animateExtraButtons(toHide: true)
        let status = PHPhotoLibrary.authorizationStatus()
        if (status == .authorized || status == .notDetermined) {
            self.imagePicker.sourceType = .savedPhotosAlbum;
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func selectCamera(_ sender: Any) {
        self.animateExtraButtons(toHide: true)
        let status = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        if (status == .authorized || status == .notDetermined) {
            self.imagePicker.sourceType = .camera
            self.imagePicker.allowsEditing = false
            self.present(self.imagePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func selectLocation(_ sender: Any) {
        self.canSendLocation = true
        self.animateExtraButtons(toHide: true)
        if self.checkLocationPermission() {
            self.locationManager.startUpdatingLocation()
        } else {
            self.locationManager.requestWhenInUseAuthorization()
        }
    }
    
    @IBAction func showOptions(_ sender: Any) {
        self.animateExtraButtons(toHide: false)
    }
    
    
    
    //MARK: NotificationCenter handlers
    func showKeyboard(notification: Notification) {
        if let frame = notification.userInfo![UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let height = frame.cgRectValue.height
            self.tableView.contentInset.bottom = height
            self.tableView.scrollIndicatorInsets.bottom = height
            if self.items.count > 0 {
                self.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: true)
            }
        }
    }
    
    //MARK: Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView.isDragging {
            cell.transform = CGAffineTransform.init(scaleX: 0.5, y: 0.5)
            UIView.animate(withDuration: 0.3, animations: {
                cell.transform = CGAffineTransform.identity
            })
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch self.items[indexPath.row].owner {
        case .receiver:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Receiver", for: indexPath) as! ReceiverCell
            cell.clearCellData()
            let currLogUser = Auth.auth().currentUser?.uid;
            let ref = Database.database().reference()
            //cell.currPic.image = self.currentUser?.profilePic
            ref.child("User").child(currLogUser!).observeSingleEvent(of: .value, with: { snapshot in
                
                if !snapshot.exists() {
                    
                    //print("No snapshot")
                }
                else{
                    if let value = snapshot.value as? [String: AnyObject] {
                        let username = value["username"] as? String ?? ""
                        let pictureurl = value["picture"] as? String ?? ""
                       // print(username)
                        //print(pictureurl)
                        
                        if let url = NSURL(string: pictureurl) {
                            if let imagedata = NSData(contentsOf: url as URL) {
                                cell.currPic.image = UIImage(data: imagedata as Data)
                            }
                        }
                    }
                }
                
            })
            //let tap = UITapGestureRecognizer(target: self, action: //#selector(ChatVC.handleTapReceiver))
            //cell.addGestureRecognizer(tap)
            switch self.items[indexPath.row].type {
            case .text:
                cell.message.text = self.items[indexPath.row].content as! String
                let v = self.items[indexPath.row].content as! String
                messageAsNote = messageAsNote + " " + v
            //self.MessageRowSelected = indexPath.row
            case .photo:
                if let image = self.items[indexPath.row].image {
                    cell.messageBackground.image = image
                    cell.message.isHidden = true
                }
            case .location:
                cell.messageBackground.image = UIImage.init(named: "location")
                cell.message.isHidden = true
            }
//            let tap = UITapGestureRecognizer(target: self, action: #selector(ChatVC.handleTapReceiver))
//            cell.messageBackground.addGestureRecognizer(tap)
//            let longPress = UILongPressGestureRecognizer(target:self, action: #selector(ChatVC.showCustomMenuForComments))
//            cell.messageBackground.addGestureRecognizer(longPress)
            
            return cell
        case .sender:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Sender", for: indexPath) as! SenderCell
            cell.clearCellData()
            cell.profilePic.image = self.currentUser?.profilePic
            cell.currUser.text = self.currentUser?.name
            
            
            switch self.items[indexPath.row].type {
            case .text:
                //print(self.items[indexPath.row].timestamp);
                cell.message.text = self.items[indexPath.row].content as! String
                messageAsNote = self.items[indexPath.row].content as! String + "\r\n"
                
                let date = NSDate(timeIntervalSince1970: Double(self.items[indexPath.row].timestamp))
                
                let dayTimePeriodFormatter = DateFormatter()
                dayTimePeriodFormatter.dateFormat = "MMM dd hh:mm a"
                
                let dateString = dayTimePeriodFormatter.string(from: date as Date)
                cell.date.text = dateString
                if(self.items[indexPath.row].isPraised == false){
                    
                    //change here
                    //cell.praised.isHidden = true;
                    //print("Text: is hidden value is changed to true");
                }
                else{
                    //print("Text: is hidden value has been changed to false");
                    //cell.praised.isHidden = false;
                }
                
                
            case .photo:
                if let image = self.items[indexPath.row].image {
                    cell.messageBackground.image = image
                    cell.message.isHidden = true
                    if(self.items[indexPath.row].isPraised == false){
                        
                        //change here
                        cell.praised.isHidden = true;
                        //print("Text: is hidden value is changed to true");
                    }
                    else{
                        //print("Text: is hidden value has been changed to false");
                        cell.praised.isHidden = false;
                    }
                }
            case .location:
                cell.messageBackground.image = UIImage.init(named: "location")
                cell.message.isHidden = true
            }
//            let tap = UITapGestureRecognizer(target: self, action: #selector(CommentsVC.handleTapSender))
//            cell.messageBackground.addGestureRecognizer(tap)
//            let longPress = UILongPressGestureRecognizer(target:self, action: #selector(CommentsVC.showCustomMenu))
//            cell.messageBackground.addGestureRecognizer(longPress)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.inputTextField.resignFirstResponder()
        switch self.items[indexPath.row].type {
        case .photo:
            if let photo = self.items[indexPath.row].image {
                let info = ["viewType" : ShowExtraView.preview, "pic": photo] as [String : Any]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showExtraView"), object: nil, userInfo: info)
                self.inputAccessoryView?.isHidden = true
            }
        case .location:
            let coordinates = (self.items[indexPath.row].content as! String).components(separatedBy: ":")
            let location = CLLocationCoordinate2D.init(latitude: CLLocationDegrees(coordinates[0])!, longitude: CLLocationDegrees(coordinates[1])!)
            let info = ["viewType" : ShowExtraView.map, "location": location] as [String : Any]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showExtraView"), object: nil, userInfo: info)
            self.inputAccessoryView?.isHidden = true
        default: break
        }
    }
    //TextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        disableCustomMenu();
        
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        
        return true;
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        
        return true;
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        return true;
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return true;
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            //self.composeMessage(type: .photo, content: pickedImage)
        } else {
            let pickedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
            //self.composeMessage(type: .photo, content: pickedImage)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.locationManager.stopUpdatingLocation()
        if let lastLocation = locations.last {
            if self.canSendLocation {
                let coordinate = String(lastLocation.coordinate.latitude) + ":" + String(lastLocation.coordinate.longitude)
                let message = Message.init(messageId: nil, fromID: "", toID: "", type: .location, content: coordinate, owner: .sender, timestamp: Int(Date().timeIntervalSince1970), isRead: false, isPraised: false, comments: ["Test"])
                Message.send(message: message, toID: self.currentUser!.id, completion: {(_) in
                })
                self.canSendLocation = false
            }
        }
    }
    
    
    //MARK: ViewController lifecycle
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.inputBar.backgroundColor = UIColor.clear
        self.view.layoutIfNeeded()
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 131/255, green: 109/255, blue: 167/255, alpha: 1)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        NotificationCenter.default.addObserver(self, selector: #selector(ChatVC.showKeyboard(notification:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
    }
    /*********************************************************************/
    func disableCustomMenu() {
        
        UIMenuController.shared.menuItems = nil
    }
    
   /* func handleTapSender(tapGestureRecognizer: UITapGestureRecognizer){
        
        let touchPoint = tapGestureRecognizer.location(in: tableView)
        if let indexPath = tableView.indexPathForRow(at: touchPoint) {
            // your code here, get the row for the indexPath or do whatever you want
            print(indexPath);
            let messageId = self.items[indexPath.row].messageId;
            let cell  = tableView.cellForRow(at: indexPath) as! SenderCell
            print(cell.message.text);
            //self.messageToBeSent = self.items[indexPath.row];
            print(messageId)
            //self.performSegue(withIdentifier: "commentsSegue", sender: nil)
            
            
            
        }
    }
    
    
    
    func handleTapReceiver(tapGestureRecognizer: UITapGestureRecognizer){
        
        let touchPoint = tapGestureRecognizer.location(in: tableView)
        if let indexPath = tableView.indexPathForRow(at: touchPoint) {
            // your code here, get the row for the indexPath or do whatever you want
            print(indexPath);
            let messageId = self.items[indexPath.row].messageId;
            let cell  = tableView.cellForRow(at: indexPath) as! ReceiverCell
            print(cell.message.text);
            //self.messageToBeSent = self.items[indexPath.row];
            print(messageId)
            
            
            
        }
    }
    func praiseMessage()
    {
        //print(self.longPressMessageRowSelected)
        //print(self.items[self.longPressMessageRowSelected].messageId)
        //print(self.items[self.longPressMessageRowSelected].toID)
        
        var selectedRowPraiseStatusFlag = self.items[self.longPressMessageRowSelected].isPraised;
        if(selectedRowPraiseStatusFlag == true){
            Message.praiseMessage(praiseStatusFlag: false, fromID: self.items[self.longPressMessageRowSelected].fromID! , toID: self.items[self.longPressMessageRowSelected].toID!, messageID: self.items[self.longPressMessageRowSelected].messageId!, completion: {_ in self.items[self.longPressMessageRowSelected].isPraised = false
                let indexPath = IndexPath(item: self.longPressMessageRowSelected, section: 0)
                self.tableView.reloadRows(at: [indexPath], with: .none)
                
            })
        }
        else{
            Message.praiseMessage(praiseStatusFlag: true, fromID: self.items[self.longPressMessageRowSelected].fromID! ,toID: self.items[self.longPressMessageRowSelected].toID!,messageID: self.items[self.longPressMessageRowSelected].messageId!, completion: {_ in self.items[self.longPressMessageRowSelected].isPraised = true
                let indexPath = IndexPath(item: self.longPressMessageRowSelected, section: 0)
                self.tableView.reloadRows(at: [indexPath], with: .none)
            })
        }
        
        
        print("Message Praised");
    }
    func commentMessage()
    {
        let alert = UIAlertController(title: "", message: "alert disappears after 5 seconds", preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)
        
        // change to desired number of seconds (in this case 5 seconds)
        let when = DispatchTime.now() + 5
        DispatchQueue.main.asyncAfter(deadline: when){
            // your code with delay
            alert.dismiss(animated: true, completion: nil)
        }
        print("Comment Please");
    }
    func copyMessage()
    {
        UIPasteboard.general.string = "Hello world"
        print("Copied");
    }
    func pasteMessage()
    {
        print("Comment Please");
    }
    
    */
    
    /*func showCustomMenu(longPressGestureRecognizer: UILongPressGestureRecognizer) {
        let touchPoint = longPressGestureRecognizer.location(in: tableView)
        if let indexPath = tableView.indexPathForRow(at: touchPoint) {
            // your code here, get the row for the indexPath or do whatever you want
            //print(indexPath);
            let messageId = self.items[indexPath.row].messageId;
            self.longPressMessageRowSelected = indexPath.row;
            
        }
        if longPressGestureRecognizer.state == .began {
            self.becomeFirstResponder()
            //self.viewForReset = longPressGestureRecognizer.view
            
            // Configure the menu item to display
            let menuItemTitle1 = NSLocalizedString("Praise", comment: "Praise menu item title")
            let menuItemTitle2 = NSLocalizedString("Comment", comment: "Comment menu item title")
            let menuItemTitle3 = NSLocalizedString("Copy", comment: "Comment menu item title")
            let menuItemTitle4 = NSLocalizedString("Paste", comment: "Comment menu item title")
            //
            let action1 = #selector(CommentsVC.praiseMessage)
            let action2 = #selector(CommentsVC.commentMessage)
            let action3 = #selector(CommentsVC.copyMessage)
            let action4 = #selector(CommentsVC.pasteMessage)
            
            let praiseMenuItem = UIMenuItem(title: menuItemTitle1, action: action1)
            let commentMenuItem = UIMenuItem(title: menuItemTitle2, action: action2)
            let copyMenuItem = UIMenuItem(title: menuItemTitle3, action: action3)
            let pasteMenuItem = UIMenuItem(title: menuItemTitle4, action: action4)
            
            
            // Configure the shared menu controller
            
            let menuController = UIMenuController.shared
            menuController.menuItems = [praiseMenuItem,commentMenuItem, copyMenuItem, pasteMenuItem]
            
            
            // Set the location of the menu in the view.
            let location = longPressGestureRecognizer.location(in: longPressGestureRecognizer.view)
            let menuLocation = CGRect(x: location.x, y: location.y, width: 0, height: 0)
            menuController.setTargetRect(menuLocation, in: longPressGestureRecognizer.view!)
            
            // Show the menu.
            menuController.setMenuVisible(true, animated: true)
        }
    }*/
    /*************************************************************************/
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        Message.markMessagesRead(forUserID: self.currentUser!.id)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.inputTextField.delegate = self;
        self.inputTextField.isUserInteractionEnabled = true;
//        for element in self.messageReceived.comments {
//            print(element)
//        }
        
        self.customization()
        self.fetchData()
    }
}



