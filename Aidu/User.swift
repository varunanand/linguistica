//  MIT License

//  Copyright (c) 2017 Haik Aslanyan

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.


import Foundation
import UIKit
import Firebase

class User: NSObject {
    
    //MARK: Properties
    let name: String
    let email: String
    let id: String
    var profilePic: UIImage
    
    //MARK: Methods
    class func registerUser(withName: String, email: String, password: String, profilePic: UIImage, completion: @escaping (Bool) -> Swift.Void) {
        Auth.auth().createUser(withEmail: email, password: password, completion: { (user, error) in
            if error == nil {
                user?.sendEmailVerification(completion: nil)
                let storageRef = Storage.storage().reference().child("usersProfilePics").child(user!.uid)
                let imageData = UIImageJPEGRepresentation(profilePic, 0.1)
                storageRef.putData(imageData!, metadata: nil, completion: { (metadata, err) in
                    if err == nil {
                        let path = metadata?.downloadURL()?.absoluteString
                        let values = ["name": withName, "email": email, "profilePicLink": path!]
                        Database.database().reference().child("users").child((user?.uid)!).child("credentials").updateChildValues(values, withCompletionBlock: { (errr, _) in
                            if errr == nil {
                                let userInfo = ["email" : email, "password" : password]
                                UserDefaults.standard.set(userInfo, forKey: "userInformation")
                                completion(true)
                            }
                        })
                    }
                })
            }
            else {
                completion(false)
            }
        })
    }
    
    class func loginUser(withEmail: String, password: String, completion: @escaping (Bool) -> Swift.Void) {
        Auth.auth().signIn(withEmail: withEmail, password: password, completion: { (user, error) in
            if error == nil {
                let userInfo = ["email": withEmail, "password": password]
                UserDefaults.standard.set(userInfo, forKey: "userInformation")
                completion(true)
            } else {
                completion(false)
            }
        })
    }
    
    class func logOutUser(completion: @escaping (Bool) -> Swift.Void) {
        do {
            try Auth.auth().signOut()
            UserDefaults.standard.removeObject(forKey: "userInformation")
            completion(true)
        } catch _ {
            completion(false)
        }
    }
    
    class func info(forUserID: String, completion: @escaping (User) -> Swift.Void) {
        Database.database().reference().child("User").child(forUserID).observeSingleEvent(of: .value, with: { (snapshot) in
            
            print(forUserID)
            print(snapshot.value)
            if let data = snapshot.value as? [String: AnyObject] {
                print("!!!!!!!!!")
                let name = data["username"]! as! String
                let email = data["email"]! as! String
                let link = URL.init(string: data["picture"]! as! String)
                //print(name)
                //print(email)
                //print(link)
                URLSession.shared.dataTask(with: link!, completionHandler: { (data, response, error) in
                    if error == nil {
                        let profilePic = UIImage.init(data: data!)
                        let user = User.init(name: name , email: email , id: forUserID, profilePic: profilePic!)
                        completion(user)
                    }
                }).resume()
            }
        })
    }
    
    class func downloadAllContacts(forUser: String, completion: @escaping (User) -> Swift.Void) {
        Database.database().reference().child("AiduUsers").child(forUser).child("contacts").observe(.childAdded, with: { (snapshot) in
            let id = snapshot.key
            print(snapshot.value)
            
            let contact = snapshot.value as! String
            //let availableContacts = data["contacts"] as! [String]
            //for contact in availableContacts
            //{
            /*****/
                Database.database().reference().child("User").child(contact).observeSingleEvent(of: .value, with: { (snapshot) in
                    if(snapshot.exists()){
                        let id = snapshot.key
                        let data = snapshot.value as! [String: Any]
                        let name = data["username"]! as! String
                        let email = data["email"]! as! String
                        let link = URL.init(string: data["picture"]! as! String)
                        URLSession.shared.dataTask(with: link!, completionHandler: { (data, response, error) in
                            if error == nil {
                                let profilePic = UIImage.init(data: data!)
                                let user = User.init(name: name, email: email, id: id, profilePic: profilePic!)
                                completion(user)
                            }
                        }).resume()
                        
                    }
                   
                    
                })
            /*****/
            //}
           
        })
    }
    
    class func downloadAllPendingRequests(forUser: String, completion: @escaping (User) -> Swift.Void) {
        Database.database().reference().child("AiduUsers").child(forUser).child("receivedRequests").observe(.childAdded, with: { (snapshot) in
            if(snapshot.exists()){
                let id = snapshot.key
                print(snapshot.value)
                
                let contact = snapshot.value as! String
                //let availableContacts = data["contacts"] as! [String]
                //for contact in availableContacts
                //{
                /*****/
                Database.database().reference().child("User").child(contact).observeSingleEvent(of: .value, with: { (snapshot) in
                    let id = snapshot.key
                    let data = snapshot.value as! [String: Any]
                    let name = data["username"]! as! String
                    let email = data["email"]! as! String
                    let link = URL.init(string: data["picture"]! as! String)
                    URLSession.shared.dataTask(with: link!, completionHandler: { (data, response, error) in
                        if error == nil {
                            let profilePic = UIImage.init(data: data!)
                            let user = User.init(name: name, email: email, id: id, profilePic: profilePic!)
                            completion(user)
                        }
                    }).resume()
                    
                })
                
            }
           
            /*****/
            //}
            
        })
    }
    
    class func checkUserVerification(completion: @escaping (Bool) -> Swift.Void) {
        Auth.auth().currentUser?.reload(completion: { (_) in
            let status = (Auth.auth().currentUser?.isEmailVerified)!
            completion(status)
        })
    }
    
    
    //MARK: Inits
    init(name: String, email: String, id: String, profilePic: UIImage) {
        self.name = name
        self.email = email
        self.id = id
        self.profilePic = profilePic
    }
}

