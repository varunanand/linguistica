//
//  InputNameViewController.swift
//  Aidu
//
//  Created by varun murali on 10/07/17.
//  Copyright © 2017 Aidu. All rights reserved.
//

import UIKit


class InputNameViewController: UIViewController {
    var profile = ProfileData();
    
    @IBOutlet weak var Name: UILabel!
    @IBOutlet weak var NameInput: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.Name.text="Are you " + profile.name! + " ?";
        self.Name.attributedText = self.attributedText(first: "Are you ", second: profile.name!, third: " ?")
        //self.TestInput.attributedText = self.attributedText(first: "Are you ", second:"Frank Dolce", third: " ?")
        

        // Do any additional setup after loading the view.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tap)

    }
    
    //function for label 
    func attributedText (first:String, second:String, third:String)  -> NSAttributedString{
        let string = first + second + third
        let result = NSMutableAttributedString(string: string)
        //      let attributesForSecondWord = [
        //          NSFontAttributeName : UIFont.italicSystemFont(ofSize: 24).boldSystemFont(ofSize: 24)
        //     ]
        
        result.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 24)  , range: NSMakeRange(first.characters.count, second.characters.count))
        result.addAttribute(NSFontAttributeName, value: UIFont.italicSystemFont(ofSize: 24)  , range: NSMakeRange(first.characters.count , second.characters.count))
        
        return NSAttributedString(attributedString: result)
    }
    //Calls this function when the tap is recognized.
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   

    @IBOutlet weak var Bugger: UIButton!
    @IBAction func Next(_ sender: Any) {
        if(NameInput.text != ""){
            profile.name = NameInput.text;
        }
        performSegue(withIdentifier: "next", sender: self)
    }
        override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "next" {
            let controller = segue.destination as! PictureViewController
            controller.profile = profile;
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
