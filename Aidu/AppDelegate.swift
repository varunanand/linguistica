//
//  AppDelegate.swift
//  Aidu
//
//  Created by sriram jaikrishnan on 10/07/17.
//  Copyright © 2017 Aidu. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import UserNotifications
import FBSDKLoginKit
import CoreLocation
import FirebaseDatabase
import FirebaseMessaging

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate ,UNUserNotificationCenterDelegate,CLLocationManagerDelegate {

    var window: UIWindow?
    var isFriend = false;
    var requestReceived = false;
    var requestSent = false;
    let manager = CLLocationManager();
    var userLogged: String = "";
    var activeUserFlag = false;

    func application(_ application: UIApplication,
                     willFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool{
        /*Location Monitoring*/
        manager.delegate=self
        manager.requestAlwaysAuthorization();
        let title = "CMU"
        
        // manager.distanceFilter= 1000
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestLocation()
        //manager.startUpdatingLocation()
        //manager.allowsBackgroundLocationUpdates = true;
        //manager.stopUpdatingLocation()
        //manager.pausesLocationUpdatesAutomatically = false;
        //manager.startMonitoringSignificantLocationChanges();
        
        let coordinate = CLLocationCoordinate2DMake(40.4428 , -79.9430  )// 36.314551108550575,-94.1977472138537
        let regionRadius = 1000.0
        let region = CLCircularRegion(center: CLLocationCoordinate2D(latitude: coordinate.latitude,
                                                                     longitude: coordinate.longitude), radius: regionRadius, identifier: title)
        
        region.notifyOnEntry = true
        region.notifyOnExit = true
        
        manager.startMonitoring(for: region)
        manager.requestState(for: region)
        return true
    }
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        //NavigationController.NavigationBar.BarStyle = UIBarStyle.white;
        //UINavigationBar.appearance().barStyle = .blackOpaque
     
        
      
        FirebaseApp.configure()
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        let refer = Database.database().reference()
        if(Auth.auth().currentUser != nil){
        let currentlyLoggedInUser = Auth.auth().currentUser?.uid;
        userLogged=Auth.auth().currentUser!.uid as String
        if(currentlyLoggedInUser != nil){
            let myUser = currentlyLoggedInUser
            let tokenVal = InstanceID.instanceID().token()
            let myToken: [String: AnyObject?] = [myUser!: tokenVal as AnyObject];
            refer.child("tokens").updateChildValues(myToken);
        }
        }
        else{
            // do nothing
        }
      
        
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
          // print("access not granted")
          //  print("error" ,error)
          //  print("granted" , granted)
        }
        if(application.isRegisteredForRemoteNotifications){
           // print("should not enter")
        }
        application.registerForRemoteNotifications()
        if(application.isRegisteredForRemoteNotifications){
           // print("should enter")
        }
        
        return true
        
    }
    
    
        
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
       //print("fn1")
        //print(userInfo)
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        //print("the data is " )
        //print(Data())
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        //print("fn2 ")
        
        // Print full message.
       // print(userInfo["aps"])
        
        let arr = userInfo[AnyHashable("aps")] as? NSDictionary
        //print("the arr is " ,arr)
        let alert = arr?["alert"] as? NSDictionary
        //print("the alert is " , alert)
        let title = alert!["title"] as? NSString;
        //print("the title is " , title)
//        if alert?.range(of:"FriendsRequest") != nil {
//            print("exists")
//
//            let sb = UIStoryboard(name: "Main", bundle: nil)
//            let otherVC = sb.instantiateViewController(withIdentifier: "faceToFace") as! FaceToFaceViewController
//            otherVC.person = alert as! String;
//
//            window?.rootViewController = otherVC;
//        }
        if (title == "Face To Face Request") {
           // print("Face to face request")
            
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let otherVC = sb.instantiateViewController(withIdentifier: "faceToFace") as! FaceToFaceViewController
            otherVC.person = title as! String;
            
            window?.rootViewController = otherVC;
        }
        if (title == "FriendsRequest") {
            //print("Face to face request")
            
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let otherVC = sb.instantiateViewController(withIdentifier: "customTab") as! CustomTabBarController
            otherVC.selectedIndex = 1;
            window?.rootViewController = otherVC;
        }
       // print("Checking if face to face has been called")
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        let handled: Bool = FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        // Add any custom logic here.
        return handled
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    

    func applicationDidEnterBackground(_ application: UIApplication) {
       
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        manager.startMonitoringSignificantLocationChanges()
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Aidu")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion) {
        if Auth.auth().currentUser != nil {
            userLogged=(Auth.auth().currentUser?.uid)!;
        }

        // only notify user if user is still within this region.

        if state == CLRegionState.inside{
            Database.database().reference().child("AiduUsers").child(userLogged).child("inAiduArea").setValue(true)
            manager.pausesLocationUpdatesAutomatically = false;
            manager.startMonitoringSignificantLocationChanges()
        }
        else{

            Database.database().reference().child("AiduUsers").child(userLogged).child("inAiduArea").setValue(false)
            manager.stopMonitoringSignificantLocationChanges()


        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location =  locations[0];
        // let span : MKCoordinateSpan = MKCoordinateSpanMake(0.01, 0.01);
        
        let myLocation : CLLocationCoordinate2D = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
        if(Auth.auth().currentUser != nil)
        { userLogged=(Auth.auth().currentUser?.uid)!;
        Database.database().reference().child("AiduUsers").child(userLogged).child("longitude").setValue(location.coordinate.longitude);
        Database.database().reference().child("AiduUsers").child(userLogged).child("latitude").setValue(location.coordinate.latitude);
        }
        
        
        
    }
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        if(Auth.auth().currentUser != nil)
        {
        userLogged=(Auth.auth().currentUser?.uid)!;
        Database.database().reference().child("AiduUsers").child(userLogged).child("inAiduArea").setValue("true")
        }
        //activeUserFlag = true;
        manager.pausesLocationUpdatesAutomatically = false;
        manager.startMonitoringSignificantLocationChanges()
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, didExitRegion: CLRegion) {
        if(Auth.auth().currentUser != nil)
        {
        userLogged=(Auth.auth().currentUser?.uid)!;
    Database.database().reference().child("AiduUsers").child(userLogged).child("inAiduArea").setValue("false")
        }
        manager.stopMonitoringSignificantLocationChanges()
         //activeUserFlag = false;
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: NSError) {
        
    }
}

