//
//  ProfilePageViewController.swift
//  Aidu
//
//  Created by Aidu on 7/26/17.
//  Copyright © 2017 Aidu. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseMessaging
import CoreLocation
import Firebase;


class ProfilePageViewController: UIViewController,CLLocationManagerDelegate {
    @IBOutlet weak var talkwithlabelconstraint: NSLayoutConstraint!
    
    @IBOutlet weak var biolabelconstraint: NSLayoutConstraint!
    var ref: DatabaseReference!
    var users = [UsersOnTheMapView]();
    var currentUserObject: UsersOnTheMapView?;
    var currentUser: String?;
    var isFriend = false;
    var requestReceived = false;
    var requestSent = false;
    var buffer = ""
    var isLabelAtMaxHeight = false
    var isLabelAtMaxHeightBio = false
    var myFirstLanguage = ""
    var mySecondLanguage = ""
    var userLogged=Auth.auth().currentUser!.uid as String;
    var overlay : UIView?
    var overlayView = UIView()
    var activityIndicator = UIActivityIndicatorView()
    var selectedUser: User?
   
    
    @IBOutlet weak var crossCulturePoints: UILabel!
    @IBOutlet weak var TalkwithHeading: UILabel!
    //reject button
    @IBOutlet weak var rejectButton: UIButton!
    
    //accept button
    @IBOutlet weak var acceptButton: UIButton!
    //awaiting label
    @IBOutlet weak var awaitingLabel: UILabel!
    //awaiting button
    @IBOutlet weak var awaitingButton: UIButton!
    //reject label
    @IBOutlet weak var rejectLabel: UILabel!
    //accept label
    @IBOutlet weak var viewMore: UIButton!
    @IBOutlet weak var acceptLabel: UILabel!
    @IBOutlet weak var ContactIcon: UIImageView!
    @IBOutlet weak var LangIcon: UIImageView!
    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet weak var SchoolLabel: UILabel!
    @IBOutlet weak var PlaceLabel: UILabel!
    @IBOutlet weak var L2Label: UILabel!
    @IBOutlet weak var NativeLabel: UILabel!
    @IBOutlet weak var TalkWithLabel: UILabel!
    @IBOutlet weak var ProfilePic: UIImageView!
    @IBOutlet weak var BioLabel: UILabel!
    @IBOutlet weak var LearningLabel: UILabel!
    @IBOutlet weak var ContactLabel: UILabel!
    @IBOutlet weak var InterestsLabel: UILabel!
    @IBOutlet weak var CrossConnectLabel: UILabel!
    @IBOutlet weak var ChatIcon: UIButton!
    @IBOutlet weak var ChatLabel: UILabel!
    @IBOutlet weak var ConnectIcon: UIButton!
    @IBOutlet weak var viewinfo: UIButton!
    @IBOutlet weak var AddContactIcon: UIButton!
    @IBOutlet weak var AddContactLabel: UILabel!
    @IBAction func dropDown(_ sender: Any) {
        performSegue(withIdentifier: "backMap", sender: nil)
    }
    
    @IBAction func startChatting(_ sender: Any) {
        //tabBarController?.viewControllers?.forEach { $0.view }
        print(self.currentUserObject?.userName)
        
        if let id = self.currentUserObject?.userId {
            User.info(forUserID: id, completion: {[weak weakSelf = self] (user) in
                DispatchQueue.main.async {
                   self.selectedUser = user
                    let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "customTab") as! CustomTabBarController
                    VC1.selectedIndex = 2;
                    let VC2 = self.storyboard!.instantiateViewController(withIdentifier: "Chat") as! ChatVC
                    let navController = UINavigationController(rootViewController: VC1) // Creating a navigation controller with VC1 at the root of the navigation stack.
                    
                   VC2.currentUser = self.selectedUser
                    VC2.calledFromProfile = true
                    let VC3 = self.storyboard!.instantiateViewController(withIdentifier: "customTab") as! CustomTabBarController
                    VC2.tabViewToBeCalled = VC3
                    navController.viewControllers = [VC2]
                    self.present(navController, animated:true, completion: nil)
                    //self.performSegue(withIdentifier: "goToChat", sender: self)
                    //let userInfo = ["user": self.selectedUser]
                    //self.tabBarController?.selectedIndex = 0
                    //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showUserMessages"), object: nil, userInfo: userInfo)
                    weakSelf = nil
                }
            })
        }
        //self.performSegue(withIdentifier: "goToChat", sender: self)
    }
   /* override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToChat" {
            let vc = segue.destination as! ChatVC
            vc.currentUser = self.selectedUser
//            print("printing values before chat ")
//            print(vc.currentUser?.id)
//            print(self.currentUserObject?.userId)
        }
    }*/
    
    @IBOutlet weak var scrollViewProfile: UIScrollView!
    
    @IBAction func acceptRequest(_ sender: Any) {
        let ref = Database.database().reference()
        var contactsUserAccepted = [String]();
        var contactsUserWhoSent = [String]();
        var userWhoAccepted = userLogged as String;
        var userWhoSent = currentUserObject?.userId as! String;
        //Add both into their respective contacts
        
       
        ref.child("AiduUsers").child(userWhoAccepted).child("contacts").observeSingleEvent(of: .value, with: { (snapshot) in
            
            if snapshot.exists() {
                 print("Coming here 123")
                
                print("appended")
                let enumeratorlist = snapshot.children
                while let restlist = enumeratorlist.nextObject() as? DataSnapshot {
                    contactsUserAccepted.append(restlist.value as! String);
                }
                //modified... nirav.. bought in the following two lines to mimic synchronous operation
                contactsUserAccepted.append(userWhoSent);
                ref.child("AiduUsers").child(userWhoAccepted).child("contacts").setValue(contactsUserAccepted);
                
            }
            else{
                contactsUserAccepted.append(userWhoSent);
                ref.child("AiduUsers").child(userWhoAccepted).child("contacts").setValue(contactsUserAccepted);
            }
            
        })
       
        ref.child("AiduUsers").child(userWhoSent).child("contacts").observeSingleEvent(of: .value, with: { (snapshot) in
            
            if snapshot.exists() {
                //print("Getting Sent Requests")
                
                print("appended")
                let enumeratorlist = snapshot.children
                while let restlist = enumeratorlist.nextObject() as? DataSnapshot {
                    contactsUserWhoSent.append(restlist.value as! String);
                }
                //modified... nirav.. bought in the following two lines to mimic synchronous operation
                contactsUserWhoSent.append(userWhoAccepted);
                ref.child("AiduUsers").child(userWhoSent).child("contacts").setValue(contactsUserWhoSent);
                
            }
            else{
                contactsUserWhoSent.append(userWhoAccepted);
                ref.child("AiduUsers").child(userWhoSent).child("contacts").setValue(contactsUserWhoSent);
            }
        })
        
        // Remove from the request sent from user who sent.
        var sentRequests = [String]();
        
        ref.child("AiduUsers").child(userWhoSent).child("sentRequests").observeSingleEvent(of: .value, with: { (snapshot) in
            
            if snapshot.exists() {
              //  print("Getting Sent Requests")
                let enumeratorlist = snapshot.children
                while let restlist = enumeratorlist.nextObject() as? DataSnapshot {
                    if(restlist.value as! String != userWhoAccepted){
                    sentRequests.append(restlist.value as! String);
                    }
                }
            }
        })
        ref.child("AiduUsers").child(userWhoSent).child("sentRequests").setValue(sentRequests);
        
        //remove from the user who accepted in recieved requests
           var receivedRequests = [String]();
        //Nirav - made change from user who sent to user who accepted
        ref.child("AiduUsers").child(userWhoAccepted).child("receivedRequests").observeSingleEvent(of: .value, with: { (snapshot) in
            
            if snapshot.exists() {
               // print("Getting recieved requests")
                let enumeratorlist = snapshot.children
                while let restlist = enumeratorlist.nextObject() as? DataSnapshot {
                    //change made nirav - user who accepted -> user who sent
                    if(restlist.value as! String != userWhoSent){
                        receivedRequests.append(restlist.value as! String);
                        
                    }
                }
               ref.child("AiduUsers").child(userWhoAccepted).child("receivedRequests").setValue(receivedRequests);
                
            }
        })
       // ref.child("AiduUsers").child(userWhoAccepted).child("receivedRequests").setValue(receivedRequests);
        
        
        self.performSegue(withIdentifier: "backMap", sender: nil)
        
    
    }
    @IBAction func faceToface(_ sender: Any) {
        let ref = Database.database().reference()
        var sendingUser = userLogged as String;
        var userToSend = currentUserObject?.userId as! String;
        let crossPoints = Int(self.crossCulturePoints.text!)
       // print("He has initiated a face to face call")
        //updating the value of face to face
        ref.child("AiduUsers").child(userToSend).child("faceToface").setValue(sendingUser);
        ref.child("AiduUsers").child(sendingUser).child("crossConnectPoints").setValue(crossPoints!  + 1);
        
        
        
        //sending the notification
        var myToken="";
        let userId = UUID().uuidString
        ref.child("tokens").observeSingleEvent(of: .value, with: { (snapshot) in
            
            if snapshot.exists() {
            //    print("the token snapshot exists")
                let enumeratorlist = snapshot.children
                //var bufferlist = ""
                while let restlist = enumeratorlist.nextObject() as? DataSnapshot {
                    
                 //   print("\(restlist.key ?? "")")
                    var val = restlist.key as? String;
                 //   print("Getting the token" , restlist.key)
                 //   print("Current user object is",self.currentUserObject?.userId);
                    if(val == userToSend){
                        
                        myToken = restlist.value! as! String;
                   //     print("my token is",myToken);
                   //     print("the database has been update")
                        
                        
                        ref.child("notification").child(userId).setValue(["token": myToken , "body": "You have recieved a face to face request" ,"title": "Face To Face Request"]);
                        
                    }
                    
                    
                }
            }
        })
        
    }
    
    
    @IBAction func performChat(_ sender: Any) {
    }
    @IBAction func rejectRequest(_ sender: Any) {
        
    }
    //@IBOutlet weak var Picture: UIImageView!
    
    //var ref: DatabaseReference!
    func fetchUserDetails(userId: String?)-> UsersOnTheMapView?{
        
        if(users.count>0){
            for object in users {
                if(object.userId == userId){
                    return object;
                }
                
            }
            
        }
        
        return nil;
    }
    @IBAction func nextProfile(_ sender: Any) {
        buffer = ""
        if(users.count > 0){
          //  print("user count is more than 0")
            let index = users.index(of: currentUserObject!)
            if(index! < users.count)
            {
                
                if (index! == users.count-1){
                    currentUserObject = users[0];
                }
                else{
                    currentUserObject = users[index!+1];
                }
                self.view.leftToRightAnimation(completion: {
                    DispatchQueue.main.async {
                        self.overlayView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
                        self.overlayView.center = self.view.center
                        self.overlayView.backgroundColor = UIColor(white: 0x444444, alpha: 0.7)
                        self.overlayView.clipsToBounds = true
                        self.overlayView.layer.cornerRadius = 10
                        
                        self.activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
                        self.activityIndicator.activityIndicatorViewStyle = .gray
                        self.activityIndicator.center = CGPoint(x:self.overlayView.bounds.width / 2, y:self.overlayView.bounds.height / 2)
                        
                        self.overlayView.addSubview(self.activityIndicator)
                        self.view.addSubview(self.overlayView)
                        self.activityIndicator.startAnimating()
                        UIApplication.shared.beginIgnoringInteractionEvents()
                        self.displayTheUser(userId: self.currentUserObject?.userId, completion:{
                            self.activityIndicator.stopAnimating()
                            self.overlayView.removeFromSuperview()
                            UIApplication.shared.endIgnoringInteractionEvents()
                        })
                        
                    }
                    
                })
                
            }
            
            
        }
        
        
    }
    
    //here if the user clicks on friend request, the label and image change to awaiting
    @IBAction func friendRequest(_ sender: Any) {
        
        ref = Database.database().reference()
        
     //   print("coming here")
        self.awaitingButton.isHidden = false
        self.awaitingLabel.isHidden = false
      //  print("and here")
        self.AddContactIcon.isHidden = true;
        self.AddContactLabel.isHidden = true;
        
        let userId = UUID().uuidString
        let ownerId = UUID().uuidString;
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        let message = ["timestamp": "876" , "body": "Hi check 123" , "ownerUid" : ownerId,"userUid" : userId]
        
        var sentRequest = [String]()
        var recieveRequest = [String]()
        print("value of sentrequest")
        print(sentRequest)
        
        ref.child("AiduUsers").child(userLogged).child("sentRequests").observeSingleEvent(of: .value, with: { (snapshot) in
            
            if snapshot.exists() {
           //     print("Getting Sent Requests")
                let enumeratorlist = snapshot.children
                while let restlist = enumeratorlist.nextObject() as? DataSnapshot {
                    if self.currentUserObject?.userId == restlist.value as? String{
                        
                    }
                    else {sentRequest.append(restlist.value as! String);}
                    
                }
                print("value of sentrequest in middle")
                print(sentRequest)
                sentRequest.append((self.currentUserObject?.userId)!)
                print("value of sentrequest after append")
                print(sentRequest)
                self.ref.child("AiduUsers").child(self.userLogged).child("sentRequests").setValue(sentRequest);
            }
            else{
                sentRequest.append((self.currentUserObject?.userId)!)
                print("value of sentrequest after append")
                print(sentRequest)
                self.ref.child("AiduUsers").child(self.userLogged).child("sentRequests").setValue(sentRequest);
            }
        })
        
        
        //Recieveing request
        
        ref.child("AiduUsers").child((currentUserObject?.userId)!).child("receivedRequests").observeSingleEvent(of: .value, with: { (snapshot) in
            
            if snapshot.exists() {
             //   print("Getting recv Requests")
                let enumeratorlist = snapshot.children
                while let restlist = enumeratorlist.nextObject() as? DataSnapshot {
                    if self.userLogged==restlist.value as! String{
                        
                    }
                    
                    else{recieveRequest.append(restlist.value as! String);}
                }
                recieveRequest.append(self.userLogged);
                self.ref.child("AiduUsers").child((self.currentUserObject?.userId)!).child("receivedRequests").setValue(recieveRequest);
            }
            else{
                recieveRequest.append(self.userLogged);
                self.ref.child("AiduUsers").child((self.currentUserObject?.userId)!).child("receivedRequests").setValue(recieveRequest);
            }
        })
        
        
        var myToken="";
        
        ref.child("tokens").observeSingleEvent(of: .value, with: { (snapshot) in
            
            if snapshot.exists() {
            //    print("the token snapshot exists")
                let enumeratorlist = snapshot.children
                //var bufferlist = ""
                while let restlist = enumeratorlist.nextObject() as? DataSnapshot {
                    
               //     print("\(restlist.key ?? "")")
                    var val = restlist.key as? String;
               //     print("Getting the token" , restlist.key)
               //     print("Current user object is",self.currentUserObject?.userId);
                    if(val == self.currentUserObject?.userId){
                        
                        myToken = restlist.value! as! String;
                  //      print("my token is",myToken);
                  //      print("the database has been update")
                        
                        
                        self.ref.child("notification").child(userId).setValue(["token": myToken , "body": "A friends request has been sent" ,"title": "FriendsRequest"]);
                        
                    }
                    
                    
                }
            }
        })
        
        self.requestSent = true;
        
    }
    @IBAction func previousProfile(_ sender: Any) {
        buffer = ""
        if(users.count > 0){
            
            let index = users.index(of: currentUserObject!)
            if(index! > -1)
            {
                if (index! == 0){
                    currentUserObject = users[users.count-1];
                }
                else{
                    currentUserObject = users[index!-1];
                }
                self.view.rightToLeftAnimation(completion: {
                    DispatchQueue.main.async {
                        self.overlayView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
                        self.overlayView.center = self.view.center
                        self.overlayView.backgroundColor = UIColor(white: 0x444444, alpha: 0.7)
                        self.overlayView.clipsToBounds = true
                        self.overlayView.layer.cornerRadius = 10
                        
                        self.activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
                        self.activityIndicator.activityIndicatorViewStyle = .gray
                        self.activityIndicator.center = CGPoint(x:self.overlayView.bounds.width / 2, y:self.overlayView.bounds.height / 2)
                        
                        self.overlayView.addSubview(self.activityIndicator)
                        self.view.addSubview(self.overlayView)
                        self.activityIndicator.startAnimating()
                        UIApplication.shared.beginIgnoringInteractionEvents()
                        self.displayTheUser(userId: self.currentUserObject?.userId, completion:{
                            self.activityIndicator.stopAnimating()
                            self.overlayView.removeFromSuperview()
                            UIApplication.shared.endIgnoringInteractionEvents()
                        })
                        
                    }
                    
                })
            }
            
            
        }
        
    }
    func displayTheUser(userId: String?, completion: @escaping () -> Swift.Void) {
        
        
        let ref = Database.database().reference()
       // print(userId!);
        ref.child("AiduUsers").child(userLogged).child("receivedRequests").observeSingleEvent(of: .value, with: { (snapshot) in
            
            if snapshot.exists() {
                let enumeratorlist = snapshot.children
                //var bufferlist = ""
                while let restlist = enumeratorlist.nextObject() as? DataSnapshot {
             //       print("Reading requests I have got.")
             //       print("\(restlist.value ?? "")")
                    var myReq = restlist.value as? String;
                    if(myReq == userId){
              //          print("i have changed my value to true")
                        self.requestReceived = true;
                    }
                    
                    
                }
                
            }
        })
        ref.child("User").child(userId!).observeSingleEvent(of: .value, with: { snapshot in
            
            if !snapshot.exists() {
                
             //   print("No snapshot")
            }
            var userLogged=Auth.auth().currentUser!.uid;
            
            ref.child("AiduUsers").child(userLogged).child("contacts").observeSingleEvent(of: .value, with: { (snapshot) in
                
                if snapshot.exists() {
                    if let val = snapshot.value as? [String: AnyObject] {
                        self.crossCulturePoints.text = val["crossConnectPoints"] as? String
                    }
                    let enumeratorlist = snapshot.children
                    //var bufferlist = ""
                    while let restlist = enumeratorlist.nextObject() as? DataSnapshot {
                   //     print("Reading the contact")
                   //     print("\(restlist.value ?? "")")
                        var myContact = restlist.value as? String;
                        if(myContact == userId){
                            self.isFriend = true;
                        }
                        
                        
                    }
                    
                }
            })
            
            
            
            
            //print(snapshot)
            if let value = snapshot.value as? [String: AnyObject] {
                let username = value["username"] as? String ?? ""
                let email = value["email"] as? String ?? ""
                var L1 = value["L1"] as? String ?? ""
                var localL1 = value["L1"] as? String ?? ""
                var L2 = value["L2"] as? String ?? ""
                let level = value["level"] as? String ?? ""
                var school = value["school"] as? String ?? ""
                let country = value["county"] as? String ?? ""
                let state = value["state"] as? String ?? ""
                let city = value["city"] as? String ?? ""
                var ownStory = value["OwnStory"] as? String ?? ""
                let pictureurl = value["picture"] as? String ?? ""
                
                
      //          print("textcontent is \(pictureurl )")
                
                Profile.downloadUserLangInfo(forUser: userLogged, completion: {(l1,l2) in
                    DispatchQueue.main.async {
                        self.myFirstLanguage = l1;
                        self.mySecondLanguage = l2;
                        if( self.myFirstLanguage == localL1){
                            self.LangIcon.image = UIImage(named: ("Native_added"))
                        }
                        else{
                            self.LangIcon.image = UIImage(named: ("L2_added"))
                            
                        }
                        
                    }
                })
                
                
                
                
                
                if let url = NSURL(string: pictureurl) {
                    if let imagedata = NSData(contentsOf: url as URL) {
                        self.ProfilePic.image = UIImage(data: imagedata as Data)
                        
                    }
                    else{
             //           print("No pic")
                    }
                }
                
                if( L1 == ""){
                    L1 = "L1 Not Specified"
                    self.NativeLabel.textColor = UIColor.gray
                }
                else{
                    L1 = L1 + ": "  + "Native"
                    
                }
                
                if( L2 == ""){
                    L2 = "L2 Not Specified"
                    self.L2Label.textColor = UIColor.gray
                    self.LearningLabel.text = ""
                    
                    
                }
                else{
                    self.LearningLabel.text = "Learning" + " " + L2
                    self.LearningLabel.sizeToFit()
                    L2 = L2 + ": " + level
                    
                }
                //var schoolName:String
                if( school == ""){
                    school = "School not specified"
                    self.SchoolLabel.textColor = UIColor.gray
                }
                else{
                    self.SchoolLabel.text = school
                }
                
                if((city != "") && ( state != "") && (country != "") ){
                    self.PlaceLabel.text = city + " " + state + " " + country
                    
                }
                else if(( city == "") && ( state != "") && ( country != "")){
                    self.PlaceLabel.text = state + " " + country
                }
                else if(( city != "") && (state == "") && ( country != "")){
                    self.PlaceLabel.text = city + " " + country
                    
                }
                else if(( city != "") && (state != "") && ( country == "")){
                    self.PlaceLabel.text = city + " " + state
                    
                }
                else if(( city == "") && (state == "") && ( country != "")){
                    self.PlaceLabel.text = country
                    
                }
                else if(( city != "") && (state == "") && ( country == "")){
                    self.PlaceLabel.text = city
                    
                }
                else if(( city == "") && (state != "") && ( country == "")){
                    self.PlaceLabel.text = state
                    
                }
                else{
                    self.PlaceLabel.text = ""
                }
                
                if(ownStory == ""){
                    ownStory = "Bio Not specifed"
                    self.BioLabel.textColor = UIColor.gray
                }
                else{
                    ownStory = "Bio:  \(ownStory)"
                }
                
                self.NameLabel.text = username
                self.SchoolLabel.text = school
                self.L2Label.text = "\(L2)"
                self.NativeLabel.text = "\(L1)"
                self.TalkwithHeading.text = "Talk about these with \(username)"
                self.BioLabel.text = ownStory
                
                
            }
            
            Profile.checkIfUserIsAContact(forUser: userLogged, toUser: userId!, completion: {(isContact) in
                DispatchQueue.main.async {

                    if(isContact){
                        self.isFriend = true;
                        self.ContactLabel.text = "Existing contact"
                        self.ContactIcon.image = UIImage(named: ("Tick_added"))
                        //added to make accept reject dynamic
                        self.acceptReject()
                    }
                    else
                    {
                        self.isFriend = false;
                        self.ContactLabel.text = "Non contact"
                        self.ContactIcon.image = UIImage(named: ("Non contact"))
                         //added to make accept reject dynamic
                        self.acceptReject()
                    }
                }
            })
            
            
            
            
            
        })
        
        ref.child("User").child(userId!).child("Hobby").observeSingleEvent(of: .value, with: { (snapshot) in
            
            if snapshot.exists() {
           //     print("Hello")
                
                if let dict = snapshot.value as? NSDictionary {
      //              print(dict)
                }
                
            }
            
            let displayConnect = UIImage(named: "face2face") as UIImage!
            let displayChat = UIImage(named: "chat") as UIImage!
            let displayAddContact = UIImage(named: "Add_to_contacts") as UIImage!
            let awaitingResponse = UIImage(named: "awaitingResponse") as UIImage!
            let accept = UIImage(named: "Tick_added") as UIImage!
            let reject  = UIImage(named: "Close") as UIImage!
            self.ConnectIcon.setImage(displayConnect, for: UIControlState.normal)
            self.ChatIcon.setImage(displayChat, for: UIControlState.normal)
            self.AddContactIcon.setImage(displayAddContact, for: UIControlState.normal)
            self.awaitingButton.setImage(awaitingResponse, for: UIControlState.normal)
            self.acceptButton.setImage(accept, for: UIControlState.normal)
            self.rejectButton.setImage(reject, for: UIControlState.normal)
            
        })
        
        
        Profile.downloadAllHobbies(forUser: userId!, completion: {(buf) in
            DispatchQueue.main.async {
                self.buffer = self.buffer + buf
                if(self.buffer == ""){
                    self.TalkWithLabel.text = "Not specified"
                    self.TalkWithLabel.textColor = UIColor.gray
                    self.viewMore.isHidden = true
                    
                }
                else
                {
                    
                    
                    self.TalkWithLabel.text = self.buffer
                    
                }
            }
        })
        Profile.downloadAllInterests(forUser: userId!, completion: {(buf) in
            DispatchQueue.main.async {
                self.buffer = self.buffer + buf
                if(self.buffer == ""){
                    self.TalkWithLabel.text = "Not specified"
                    self.TalkWithLabel.textColor = UIColor.gray
                    self.viewMore.isHidden = true
                    
                }
                else
                {
                    self.TalkWithLabel.text = String(self.buffer.characters.dropLast(3))
                    self.TalkWithLabel.numberOfLines = 0;
                    //self.TalkWithLabel.sizeToFit()
                    
                    
                    
                }
            }
        })
        
       
        
        
        print("slef acceptreject")
        self.acceptReject()
        completion()
    }
    func getLabelHeight(text: String, width: CGFloat, font: UIFont) -> CGFloat {
        let lbl = UILabel(frame: .zero)
        lbl.frame.size.width = width
        lbl.font = font
        lbl.numberOfLines = 0
        lbl.text = text
        lbl.lineBreakMode = NSLineBreakMode.byWordWrapping
        
        lbl.sizeToFit()
        
        return lbl.frame.size.height
    }
    
    
    @IBAction func viewMore(_ sender: Any) {
       
        if isLabelAtMaxHeight {
            self.viewMore.setTitle("View more", for: .normal)
            isLabelAtMaxHeight = false
            talkwithlabelconstraint.constant = 20
            self.TalkWithLabel.numberOfLines = 0
            self.TalkWithLabel.lineBreakMode = NSLineBreakMode.byTruncatingTail
            self.TalkWithLabel.sizeToFit()
            
        }
        else {
            self.TalkWithLabel.numberOfLines = 0
            self.TalkWithLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
            self.TalkWithLabel.sizeToFit()
            self.viewMore.setTitle("View less", for: .normal)
            isLabelAtMaxHeight = true
            talkwithlabelconstraint.constant = getLabelHeight(text: self.TalkWithLabel.text!, width: self.TalkWithLabel.bounds.width, font: self.TalkWithLabel.font)
            
        }
    }
    
    @IBAction func viewInfo(_ sender: Any) {
        if isLabelAtMaxHeightBio {
            self.viewinfo.setTitle("View more", for: .normal)
            isLabelAtMaxHeightBio = false
            biolabelconstraint.constant = 20
            self.BioLabel.numberOfLines = 0
            self.BioLabel.lineBreakMode = NSLineBreakMode.byTruncatingTail
            self.BioLabel.sizeToFit()
            
        }
        else {
            self.BioLabel.numberOfLines = 0
            self.BioLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
            self.BioLabel.sizeToFit()
            self.viewinfo.setTitle("View less", for: .normal)
            isLabelAtMaxHeightBio = true
            biolabelconstraint.constant = getLabelHeight(text: self.BioLabel.text!, width: self.BioLabel.bounds.width, font: self.BioLabel.font)
            
        }
        
    }
    override func viewDidLoad() {
        

        self.overlayView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        self.overlayView.center = view.center
        self.overlayView.backgroundColor = UIColor(white: 0x444444, alpha: 0.7)
        self.overlayView.clipsToBounds = true
        self.overlayView.layer.cornerRadius = 10
        
        self.activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        self.activityIndicator.activityIndicatorViewStyle = .gray
        self.activityIndicator.center = CGPoint(x:overlayView.bounds.width / 2, y:overlayView.bounds.height / 2)
        
        self.overlayView.addSubview(activityIndicator)
        view.addSubview(overlayView)
        activityIndicator.startAnimating()
        
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)
        
        
    }
    
    

    
    
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
                if(users.count > 0){
                    
                    let index = users.index(of: currentUserObject!)
                    if(index! > -1)
                    {
                        if (index! == 0){
                            currentUserObject = users[users.count-1];
                        }
                        else{
                            currentUserObject = users[index!-1];
                        }
                        self.view.rightToLeftAnimation(completion: {
                            DispatchQueue.main.async {
                                self.overlayView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
                                self.overlayView.center = self.view.center
                                self.overlayView.backgroundColor = UIColor(white: 0x444444, alpha: 0.7)
                                self.overlayView.clipsToBounds = true
                                self.overlayView.layer.cornerRadius = 10
                                
                                self.activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
                                self.activityIndicator.activityIndicatorViewStyle = .gray
                                self.activityIndicator.center = CGPoint(x:self.overlayView.bounds.width / 2, y:self.overlayView.bounds.height / 2)
                                
                                self.overlayView.addSubview(self.activityIndicator)
                                self.view.addSubview(self.overlayView)
                                self.activityIndicator.startAnimating()
                                UIApplication.shared.beginIgnoringInteractionEvents()
                                self.displayTheUser(userId: self.currentUserObject?.userId, completion:{
                                    self.activityIndicator.stopAnimating()
                                    self.overlayView.removeFromSuperview()
                                    UIApplication.shared.endIgnoringInteractionEvents()
                                })
                                
                            }
                            
                        })
                    }
                    
                    
                }
                
                
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
                buffer = ""
                if(users.count > 0){
                    //  print("user count is more than 0")
                    let index = users.index(of: currentUserObject!)
                    if(index! < users.count)
                    {
                        
                        if (index! == users.count-1){
                            currentUserObject = users[0];
                        }
                        else{
                            currentUserObject = users[index!+1];
                        }
                        self.view.leftToRightAnimation(completion: {
                            DispatchQueue.main.async {
                                /*self.overlay = UIView(frame: self.view.frame)
                                self.overlay!.backgroundColor = UIColor.black
                                self.overlay!.alpha = 0.8
                                self.view.addSubview(self.overlay!)
                                let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
                                let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
                                loadingIndicator.hidesWhenStopped = true
                                loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
                                loadingIndicator.startAnimating();

                                alert.view.addSubview(loadingIndicator)
                                self.present(alert, animated: true, completion: nil)
                                 UIApplication.shared.beginIgnoringInteractionEvents()*/
                                self.overlayView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
                                self.overlayView.center = self.view.center
                                self.overlayView.backgroundColor = UIColor(white: 0x444444, alpha: 0.7)
                                self.overlayView.clipsToBounds = true
                                self.overlayView.layer.cornerRadius = 10
                                
                                self.activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
                                self.activityIndicator.activityIndicatorViewStyle = .gray
                                self.activityIndicator.center = CGPoint(x:self.overlayView.bounds.width / 2, y:self.overlayView.bounds.height / 2)
                                
                                self.overlayView.addSubview(self.activityIndicator)
                                self.view.addSubview(self.overlayView)
                                self.activityIndicator.startAnimating()
                                UIApplication.shared.beginIgnoringInteractionEvents()
                                self.displayTheUser(userId: self.currentUserObject?.userId,completion:{
                                    self.activityIndicator.stopAnimating()
                                    self.overlayView.removeFromSuperview()
                                    UIApplication.shared.endIgnoringInteractionEvents()
                                    /*self.dismiss(animated: false, completion: nil)
                                    self.overlay?.removeFromSuperview()
                                    UIApplication.shared.endIgnoringInteractionEvents()*/
                                })
                                
                            }
                            
                        })
                        
                        
                    }
                    
                    
                }
            default:
                break
            }
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        
            checkIfIamInsideAiduZone(completion: decideOnShowingOutOfAreaPopup)
        
        
        
         }
    
     func checkIfIamInsideAiduZone(completion: @escaping (Bool) -> Swift.Void)  {
     userLogged=(Auth.auth().currentUser?.uid)!;
     Database.database().reference().child("AiduUsers").child(userLogged).observeSingleEvent(of: .value, with: { (snapshot) in
     
     if snapshot.exists() {
     let data = snapshot.value as! [String: Any]
     
        if((data["inAiduArea"] != nil)  ){
            let inFlag = data["inAiduArea"] as? Bool
            completion(inFlag!)}
     }
      
     })
      }
 /*
   
     */
    func decideOnShowingOutOfAreaPopup(inFlag: Bool){
        self.users = []
        if(inFlag == false){
            let alert = UIAlertController(title: "Out of Banter Area", message: "You are out of Banter Area.To protect your safety, you can only view Aidu users within Banter area", preferredStyle: UIAlertControllerStyle.alert)
            let action = UIAlertAction(title: "Find Banter Users", style: UIAlertActionStyle.default) { (_) in
                //run your function here
                alert.dismiss(animated: true, completion: nil)
                self.findAiduUsers()
                }
            
            alert.addAction(action)
            
            self.present(alert, animated: true, completion:nil )
        }
        else{
            self.findAiduUsers()
        }
    }
    func findAiduUsers(){
        
        self.overlayView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        self.overlayView.center = view.center
        self.overlayView.backgroundColor = UIColor(white: 0x444444, alpha: 0.7)
        self.overlayView.clipsToBounds = true
        self.overlayView.layer.cornerRadius = 10
        
        self.activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        self.activityIndicator.activityIndicatorViewStyle = .gray
        self.activityIndicator.center = CGPoint(x:overlayView.bounds.width / 2, y:overlayView.bounds.height / 2)
        
        self.overlayView.addSubview(activityIndicator)
        view.addSubview(overlayView)
        activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
        self.fetchUsers(completion:{
            self.activityIndicator.stopAnimating()
            self.overlayView.removeFromSuperview()
            UIApplication.shared.endIgnoringInteractionEvents()
        })
    }
    func acceptReject(){
        if(self.isFriend){
            self.CrossConnectLabel.isHidden = false;
            self.ConnectIcon.isHidden = false;
            self.ChatIcon.isHidden = false;
            self.ChatLabel.isHidden = false;
            self.AddContactIcon.isHidden = true;
            self.AddContactLabel.isHidden = true;
            self.awaitingLabel.isHidden = true;
            self.awaitingButton.isHidden = true;
            self.acceptButton.isHidden = true;
            self.acceptLabel.isHidden = true;
            self.rejectButton.isHidden = true;
            self.rejectLabel.isHidden = true;
        }
        else if(self.requestReceived){
            self.CrossConnectLabel.isHidden = true;
            self.ConnectIcon.isHidden = true;
            self.ChatIcon.isHidden = true;
            self.ChatLabel.isHidden = true;
            self.AddContactIcon.isHidden = true;
            self.AddContactLabel.isHidden = true;
            self.awaitingLabel.isHidden = true;
            self.awaitingButton.isHidden = true;
            self.acceptButton.isHidden = false;
            self.acceptLabel.isHidden = false;
            self.rejectButton.isHidden = false;
            self.rejectLabel.isHidden = false;
        }
        else{
            self.CrossConnectLabel.isHidden = true;
            self.ConnectIcon.isHidden = true;
            self.ChatIcon.isHidden = true;
            self.ChatLabel.isHidden = true;
            self.AddContactIcon.isHidden = false;
            self.AddContactLabel.isHidden = false;
            self.awaitingLabel.isHidden = true;
            self.awaitingButton.isHidden = true;
            self.acceptButton.isHidden = true;
            self.acceptLabel.isHidden = true;
            self.rejectButton.isHidden = true;
            self.rejectLabel.isHidden = true;
            
        }
        
    }
    
    func fetchUsers(completion: @escaping () -> Swift.Void)
    {
        
        
        Database.database().reference().child("AiduUsers").observe(.childAdded, with: {
            (snapshot) in
            let dataDict =  snapshot.value as! [String: AnyObject]
            print("printing data dic")
              print(dataDict);
            let user = UsersOnTheMapView()
            var isContactFlag: Bool!;
            var isNativeFlag: Bool!;
            //   print("The users are",user.inAiduArea,user.longitude)
            user.userId = snapshot.key;
            user.l1 = dataDict["l1"] as? String;
            user.l2 = dataDict["l2"] as? String;
            user.latitude = dataDict["latitude"] as! Double;
            user.longitude = dataDict["longitude"] as! Double;
            user.inAiduArea = dataDict["inAiduArea"] as! Bool;
            user.visible = dataDict["visible"] as! Bool;
            user.userName = dataDict["name"] as! String;
            
            if(user.userId != Auth.auth().currentUser!.uid)// && user.inAiduArea != false)
            {self.users.append(user)
                //completion()
            }
            print("printing users")
            print(self.users)
            DispatchQueue.main.async(execute: {

                let userLoggedIn=Auth.auth().currentUser!.uid;
                isContactFlag = true; // We need to Make sure this needs to be removed
                self.currentUserObject = self.fetchUserDetails(userId: self.users[0].userId);
                self.displayTheUser(userId: self.currentUserObject?.userId, completion:{});
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            self.acceptReject()
                            completion()
                        }
            })
      
            }, withCancel: nil)
       

        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
}
//For swipe transitions

extension UIView {
    func leftToRightAnimation(duration: TimeInterval = 0.5, completionDelegate: AnyObject? = nil, completion: @escaping () -> Swift.Void) {
        // Create a CATransition object
        let leftToRightTransition = CATransition()
        
        // Set its callback delegate to the completionDelegate that was provided
        if let delegate: AnyObject = completionDelegate {
            leftToRightTransition.delegate = delegate as? CAAnimationDelegate
        }
        
        leftToRightTransition.type = kCATransitionPush
        leftToRightTransition.subtype = kCATransitionFromRight
        leftToRightTransition.duration = duration
        leftToRightTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        leftToRightTransition.fillMode = kCAFillModeRemoved
        
        // Add the animation to the View's layer
        self.layer.add(leftToRightTransition, forKey: "leftToRightTransition")
        completion()
    }
    func rightToLeftAnimation(duration: TimeInterval = 0.5, completionDelegate: AnyObject? = nil, completion: @escaping () -> Swift.Void) {
        // Create a CATransition object
        let rightToLeftTransition = CATransition()
        
        // Set its callback delegate to the completionDelegate that was provided
        if let delegate: AnyObject = completionDelegate {
            rightToLeftTransition.delegate = delegate as? CAAnimationDelegate
        }
        
        rightToLeftTransition.type = kCATransitionPush
        rightToLeftTransition.subtype = kCATransitionFromLeft
        rightToLeftTransition.duration = duration
        rightToLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        rightToLeftTransition.fillMode = kCAFillModeRemoved
        
        // Add the animation to the View's layer
        self.layer.add(rightToLeftTransition, forKey: "rightToLeftTransition")
        completion()
    }
}




