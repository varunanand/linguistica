//
//  Hobby.swift
//  Aidu
//
//  Created by varun murali on 14/07/17.
//  Copyright © 2017 Aidu. All rights reserved.
//

import Foundation
class Hobby
{
    var title: String
    var isIntrested: Bool
    
    public init(title: String)
    {
        self.title = title
        self.isIntrested = false
        
    }
    
    
}
extension Hobby
{
    public class func getMockData() -> [Hobby]
    {
        return [
            Hobby(title: "Sports"),
            Hobby(title: "Music"),
            Hobby(title: "TV Shows"),
            Hobby(title: "Art"),
            Hobby(title: "Design"),
            Hobby(title: "Movies"),
            Hobby(title: "IT & Tech"),
            Hobby(title: "Business"),
            Hobby(title: "Politics"),
            Hobby(title: "Health"),
            Hobby(title: "Fashion"),
            Hobby(title: "Languages"),
            Hobby(title: "Travel"),
            Hobby(title: "Food"),
            Hobby(title: "Nature")
        ]
    }
}

