//
//  FaceToFaceViewController.swift
//  Aidu
//
//  Created by Elara on 11/11/17.
//  Copyright © 2017 Aidu. All rights reserved.
//

import UIKit
import FirebaseDatabase
import Firebase;

class FaceToFaceViewController: UIViewController {
    var person: String = "";
    var ref: DatabaseReference!
    @IBOutlet weak var ProfilePicture: UIImageView!
    @IBOutlet weak var AcceptText: UITextView!
    @IBOutlet weak var NameLabel: UITextView!
    
    
    @IBAction func reject(_ sender: Any) {
        let ref = Database.database().reference()
        performSegue(withIdentifier: "toMap", sender: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        var userCalling = "noValue";
        
        print("Hello boss")
        
        // Increase Cross connect points
        var userLogged=Auth.auth().currentUser!.uid as String;
        var crossConnectPoints = 0;
        let ref = Database.database().reference()
        //get the cross connect points
        ref.child("AiduUsers").child(userLogged).child("crossConnectPoints").observeSingleEvent(of: .value, with: { (snapshot) in

            if snapshot.exists() {
                let enumeratorlist = snapshot.children
                while let restlist = enumeratorlist.nextObject() as? DataSnapshot {
                    crossConnectPoints = restlist.value as! Int;
                }
            }
        })
        
        ref.child("AiduUsers").child(userLogged).child("faceToface").observeSingleEvent(of: .value, with: { (snapshot) in
            
            if snapshot.exists() {
                let enumeratorlist = snapshot.children
                while let restlist = enumeratorlist.nextObject() as? DataSnapshot {
                    userCalling = restlist.value as! String;
                }
            }
        })
        
        //increment the cross connect points
        crossConnectPoints = crossConnectPoints + 1 ;
        //insert into database.
        ref.child("AiduUsers").child(userLogged).child("crossConnectPoints").setValue(crossConnectPoints);
        //Check his user id. display his name.
        
        //if accepted cross connect points will be incremented.
        //it should redirect to map page view controller
        
        print(person);
         DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.displayTheUser(userId: userCalling);
        }
        self.contentHeading.text = person;
        // Do any additional setup after loading the view.
    }
    @IBOutlet weak var contentHeading: UITextView!
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func makeVisible(){
        print("Inside make visible function")
        let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "faceToFace") as! FaceToFaceViewController
        
        //        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //        let controller = storyboard.instantiateViewController(withIdentifier: "faceToFace")
        self.present(viewController, animated: true, completion: nil)
        
    }
    
    func displayTheUser(userId: String?) {
        
        let ref = Database.database().reference()
        print(userId!);
        
        ref.child("User").child(userId!).observeSingleEvent(of: .value, with: { snapshot in
            
            if !snapshot.exists() {
                
                print("No snapshot")
            }
            
            if let value = snapshot.value as? [String: AnyObject] {
                let username = value["username"] as? String ?? ""
                let pictureurl = value["picture"] as? String ?? ""
                print(username)
                print(pictureurl)
                self.NameLabel.text =  username + " is asking you to connect!"
                self.AcceptText.text = "Accept it and then find " + username + " to talk. By accepting Cross Culture Connect, you agree to let the other user potentially record the conversation though they should ask for permission first."
                if let url = NSURL(string: pictureurl) {
                    if let imagedata = NSData(contentsOf: url as URL) {
                        self.ProfilePicture.image = UIImage(data: imagedata as Data)
                        
                    }
                    else{
                        print("No pic")
                    }
                }
            }
        })
        
        
    }
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

