//
//  AboutViewController.swift
//  Aidu
//
//  Created by varun murali on 12/07/17.
//  Copyright © 2017 Aidu. All rights reserved.
//

import UIKit
import FirebaseDatabase

import UIKit
import MapKit;
import CoreLocation
import Firebase;
class AboutViewController: UIViewController {
    var profile = ProfileData()
    var ref: DatabaseReference!
    override func viewDidLoad() {
        super.viewDidLoad()
        print("I am about to insert into database")
        ref = Database.database().reference()
        // Do any additional setup after loading the view.
        // Do any additional setup after loading the view.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tap)
        
    }
    //Calls this function when the tap is recognized.
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    @IBOutlet weak var AboutData: UITextView!
    
  
    @IBAction func Next(_ sender: Any) {
        
        
        profile.ownStory = AboutData.text
        print(profile.name);
        print("HI BYE CHECK");
        ProfileData.uid = Auth.auth().currentUser?.uid;
        
        self.ref.child("User").child(ProfileData.uid!).setValue(["username": profile.name , "email" : profile.email, "L1" : profile.L1 ,"L2": profile.L2 , "city" : profile.city ,"county" : profile.country , "level" : profile.level , "Hobby" : profile.intrest , "intrestList" : profile.intestList , "OwnStory" : profile.ownStory , "school" : profile.school , "picture": profile.picture?.absoluteString , "state" : profile.state]);

        self.ref.child("AiduUsers").child(ProfileData.uid!).child("l1").setValue(profile.L1);
        self.ref.child("AiduUsers").child(ProfileData.uid!).child("l2").setValue(profile.L2);
        self.ref.child("AiduUsers").child(ProfileData.uid!).child("latitude").setValue(40.442833);
        self.ref.child("AiduUsers").child(ProfileData.uid!).child("longitude").setValue(-79.943032);
        self.ref.child("AiduUsers").child(ProfileData.uid!).child("inAiduArea").setValue(true);
        self.ref.child("AiduUsers").child(ProfileData.uid!).child("visible").setValue(true);
        self.ref.child("AiduUsers").child(ProfileData.uid!).child("name").setValue(profile.name);
        self.ref.child("AiduUsers").child(ProfileData.uid!).child("faceToface").setValue("noValue");
        self.ref.child("AiduUsers").child(ProfileData.uid!).child("crossConnectPoints").setValue(0);

//        
        
    performSegue(withIdentifier: "Map", sender: nil)
        
        print("Hi Sucessfully inserted");
        
    }
    
    @IBAction func Back(_ sender: Any) {
          performSegue(withIdentifier: "back", sender: self)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "Map" {
            let controller = segue.destination as! UITabBarController
            //controller.profile = profile;
        }
        if segue.identifier == "back" {
            let controller = segue.destination as! IntrestViewController
            controller.profile = profile;
        }

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
