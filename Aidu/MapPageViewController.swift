//
//  MapPageViewController.swift
//  Aidu
//
//  Created by varun murali on 05/08/17.
//  Copyright © 2017 Aidu. All rights reserved.
//

import UIKit
import MapKit;
import CoreLocation
import Firebase;

class MapPageViewController: UIViewController,MKMapViewDelegate,CLLocationManagerDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    var profile = ProfileData();
    var invisible = false;
    let manager = CLLocationManager();
    var users = [UsersOnTheMapView]();
    var userLocations = [CLLocation]();
    var contactsList = [String]();
    var languageFilter = "English"; //hardcoded for testing sake
//    let userID = ProfileData.uid;
    var location = CLLocation();
//    var currentSelectedUserOnMapView: String?
//    var entryChecker = true;
////
    @IBAction func visibilityToggle(_ sender: Any) {
        invisible = !invisible ;
        if(invisible == false){
//    Database.database().reference().child("AiduUsers").child(userID!).child("inAiduArea").setValue("true");
        }
        else{
            manager.startUpdatingLocation();
//            Database.database().reference().child(userID!).child("inAiduArea").setValue("false");
//            
        }
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapView.delegate = self;
        manager.delegate=self
        let coordinate = CLLocationCoordinate2DMake(40.452701 , -79.911321)
        let regionRadius = 20.0
        manager.desiredAccuracy=kCLLocationAccuracyBest
        manager.requestAlwaysAuthorization()
        manager.startUpdatingLocation()
        manager.stopUpdatingLocation();
        manager.startMonitoringSignificantLocationChanges();

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
