//
//  EditInterestTableViewController.swift
//  Aidu
//
//  Created by Elara on 2/15/18.
//  Copyright © 2018 Aidu. All rights reserved.
//

import UIKit

class EditInterestTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var myHobbyList = [String]()
    var myInterestList = [String]()
    
    var ArtList = Intrest.getArtData();
    var FoodList = Intrest.getFoodData();
    var SportsList = Intrest.getSportsData();
    var MovieList = Intrest.getMovieData();
    var MusicList = Intrest.getMusicData();
    var TVList = Intrest.getTVData();
    var DesignList = Intrest.getDesignData();
    var ITList = Intrest.getITData();
    var BusinessList = Intrest.getBusinessData();
    var PoliticsList = Intrest.getPoliticsData();
    var HealthList = Intrest.getHealthData();
    var FashionList = Intrest.getFashionData();
    var LanguageList = Intrest.getLanguagesData();
    var TravelList = Intrest.getTravelData();
    var NatureList = Intrest.getNatureData();
    
    var interestList = Intrest.InitialData();
    
    
    @IBAction func DoneButton(_ sender: Any) {
        
        print(myInterestList)
        performSegue(withIdentifier: "doneEditInterests", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("prinitng my interest and hobby list")
        print(myInterestList)
        print(myHobbyList)
        
        if(self.myHobbyList.contains("Art")){
            for values in ArtList
            {
                interestList.append(values);
            }
            
        }
        if(self.myHobbyList.contains("Music")){
            for values in MusicList
            {
                interestList.append(values);
            }
            
        }
        if(self.myHobbyList.contains("Sports")){
            for values in SportsList
            {
                interestList.append(values);
            }
            
        }
        if(self.myHobbyList.contains("TV Shows")){
            for values in TVList
            {
                interestList.append(values);
            }
            
        }
        if(self.myHobbyList.contains("Design")){
            for values in DesignList
            {
                interestList.append(values);
            }
            
        }
        if(self.myHobbyList.contains("Movies")){
            for values in MovieList
            {
                interestList.append(values);
            }
            
        }
        if(self.myHobbyList.contains("IT & Tech")){
            for values in ITList
            {
                interestList.append(values);
            }
            
        }
        if(self.myHobbyList.contains("Business")){
            for values in BusinessList
            {
                interestList.append(values);
            }
            
        }
        if(self.myHobbyList.contains("Politics")){
            for values in PoliticsList
            {
                interestList.append(values);
            }
        }
        if(self.myHobbyList.contains("Health")){
            for values in HealthList
            {
                interestList.append(values);
            }
            
        }
        if(self.myHobbyList.contains("Fashion")){
            for values in FashionList
            {
                interestList.append(values);
            }
            
        }
        if(self.myHobbyList.contains("Languages")){
            for values in LanguageList
            {
                interestList.append(values);
            }
            
        }
        if(self.myHobbyList.contains("Travel")){
            for values in TravelList
            {
                interestList.append(values);
            }
            
        }
        if(self.myHobbyList.contains("Food")){
            for values in FoodList
            {
                interestList.append(values);
            }
            
        }
        if(self.myHobbyList.contains("Nature")){
            for values in NatureList
            {
                interestList.append(values);
            }

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
            self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

     func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return interestList.count;
    }

     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "editInterest", for: indexPath)
        
        if indexPath.row < self.interestList.count
        {
            let item = self.interestList[indexPath.row]
            cell.textLabel?.text = item.title
            if self.myInterestList.contains(item.title){
                item.isIntrested = true
            }
            let accessory: UITableViewCellAccessoryType = item.isIntrested ? .checkmark : .none
            cell.accessoryType = accessory
        }
        
        
        
        return cell
    }
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row < interestList.count
        {
            let item = interestList[indexPath.row]
            item.isIntrested = !item.isIntrested;
            if myInterestList.contains(item.title){
                //let str = item.title as String!
                print(item.title)
                let ind =  myInterestList.index(of: item.title as String!) as Int!
                myInterestList.remove(at: ind!)
            }
            else{ myInterestList.append(item.title);}
            print(myInterestList)
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "doneEditInterests" {
            
            
            let controller = segue.destination as! UserProfileViewController
            controller.editedinterestList = myInterestList
            
            
        }
        
        
        
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
