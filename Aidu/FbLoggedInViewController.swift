//
//  FbLoggedInViewController.swift
//  Aidu
//
//  Created by varun murali on 10/07/17.
//  Copyright © 2017 Aidu. All rights reserved.
//

import UIKit

class FbLoggedInViewController: UIViewController {
    var profile=ProfileData();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
         self.performSegue(withIdentifier: "NameSegue", sender: self)
        }
        
        
        // Delay 2 seconds
                // Do any additional setup after loading the view.
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "NameSegue" {
            let controller = segue.destination as! InputNameViewController
           
            controller.profile = profile;
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
