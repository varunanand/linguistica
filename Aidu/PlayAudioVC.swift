//
//  PlayAudioVC.swift
//  Voice Recorder
//
//  Created by Elara on 10/29/17.
//  Copyright © 2017 Elara. All rights reserved.
//

import UIKit
import AVFoundation

class PlayAudioVC: UIViewController, UITableViewDataSource, UITableViewDelegate  {
    var audioPlayer:AVAudioPlayer!
    var dirPath:String! // Path to recorded files.
    var fileNames = [String]() // File names of all saved recordings.
    let textCellIdentifier = "TextCell"

    @IBOutlet weak var tableView: UITableView!
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        // Path to all stored files
        dirPath = NSSearchPathForDirectoriesInDomains(.documentDirectory,.userDomainMask,true)[0] 
        
        // Find all m4a-files
        let filemanager:FileManager = FileManager()
        let files = filemanager.enumerator(atPath: dirPath)
        let mp3Files = files!.filter{($0 as AnyObject).pathExtension == "m4a"}
        //print("mp3 urls:",mp3Files)
        for mp3File in mp3Files{
            
            fileNames.append (mp3File as! String)
        }
        
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.  
    }
    
    // This is only regarding the tableView
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return fileNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: textCellIdentifier, for: indexPath as IndexPath)
        let row = indexPath.row
        cell.textLabel?.text = fileNames[row]
        
        return cell
    }
    
    // MARK: UITableViewDelegate Methods
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
        
        let row = indexPath.row
        
        let fileToPlay = String(dirPath) + "/" + String(fileNames[row])
        var url=NSURL.fileURL(withPath: fileToPlay)
        audioPlayer = try! AVAudioPlayer(contentsOf: url)
        audioPlayer.play()  
    } 
}
