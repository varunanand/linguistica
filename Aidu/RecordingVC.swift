//
//  ViewController.swift
//  Voice Recorder
//
//  Created by Elara on 10/29/17.
//  Copyright © 2017 Elara. All rights reserved.
//

import UIKit
import AVFoundation
import Foundation

class RecordingVC:  UIViewController, AVAudioRecorderDelegate,UITextFieldDelegate,UINavigationControllerDelegate {
    var audioRecorder:AVAudioRecorder!
    var meterTimer:Timer!
    var recordedAudio:RecordedAudio!
    var recordingSession: AVAudioSession!
    var filePath:NSURL!
    @IBOutlet weak var statusLabel: UILabel!
    var peakPower: Float!
    @IBOutlet  var volumeMeter: UIProgressView!
    @IBOutlet weak var stopButton: UIButton!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var RecordLabel: UILabel!
    @IBOutlet weak var profilePic: UIImageView!

    
    //Action to record audio
    @IBAction func recordAudio(_ sender: UIButton) {
        recordButton.isEnabled = false
        stopButton.isHidden = false
        volumeMeter.isHidden = false
        RecordLabel.sizeToFit();
        RecordLabel.text = "   Recording!"
        
        let dirPath = NSSearchPathForDirectoriesInDomains(.documentDirectory,.userDomainMask,true)[0] as! String
        
        //Named the file to be unique
        var currentDateTime=NSDate();
        var formatter = DateFormatter();
        formatter.dateFormat = "yyyy-MM-dd_HHmmss";
        var recordingName = formatter.string(from: currentDateTime as Date)+".m4a"
        var pathArray = [dirPath, recordingName]
         filePath = NSURL.fileURL(withPathComponents: pathArray) as! NSURL
        print(filePath)
        recordingSession = AVAudioSession.sharedInstance()
        try! recordingSession.setCategory(AVAudioSessionCategoryPlayAndRecord,with: [.defaultToSpeaker])
        try! recordingSession.setActive(true)
        recordingSession.requestRecordPermission() { [unowned self] allowed in
            DispatchQueue.main.async {
                if allowed {
                    
                    let settings = [
                        AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                        AVSampleRateKey: 12000,
                        AVNumberOfChannelsKey: 1,
                        AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
                    ]
                    
                    do {
                        self.audioRecorder = try AVAudioRecorder(url: self.filePath! as URL, settings: settings)
                        self.audioRecorder.delegate = self
                        self.audioRecorder.isMeteringEnabled=true
                        self.audioRecorder.prepareToRecord()
                        self.audioRecorder.record()
                        
                        //repeat this thread to increment the voice recorder indicator
                        OperationQueue().addOperation({[weak self] in
                            repeat {
                                self?.audioRecorder.updateMeters()
                                
                                self?.peakPower = (self?.audioRecorder.peakPower(forChannel: 0))!
                                
                                self?.performSelector(onMainThread: #selector(self?.updateMeter), with: self, waitUntilDone: false)
                                Thread.sleep(forTimeInterval: 0.05)//20 FPS
                            }
                                while (self?.audioRecorder.isRecording)!
                        })
                        //repeat this thread to increment timer
                        DispatchQueue.main.async {
                        self.meterTimer = Timer.scheduledTimer(timeInterval: 0.1,
                                                               target:self,
                                                               selector:#selector(self.updateAudioMeter(_:)),
                                                               userInfo:nil,
                                                               repeats:true)

                        }
                        
                    } catch {
                        self.finishRecording(success: false)
                    }
                } else {
                    print("He did not allow permission")
                }
            }
        }
        
        

    }
    func finishRecording(success: Bool) {
        audioRecorder.stop()
        audioRecorder = nil
        
    }
    

    
    
    @IBAction func stopRecording(_ sender: UIButton) {
        recordButton.isEnabled=true
        audioRecorder.stop()
        print(audioRecorder.isRecording)
        recordingSession = AVAudioSession.sharedInstance()
        try! recordingSession.setActive(false)
        
       
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        stopButton.isHidden = true
        volumeMeter.isHidden=true
        statusLabel.text="00:00"
        profilePic.image = self.currentUser?.profilePic
        print(self.currentUser?.profilePic)
        self.customization();
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    //Save the audio recording to the designated folder
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool)
    {
        if(flag)
        {
            //Store in Model
            recordedAudio=RecordedAudio()
            recordedAudio.filePathURL=recorder.url as NSURL
            recordedAudio.title=recorder.url.lastPathComponent
            //Segway once we've finished processing the audio
            var tabbarController = self.tabBarController as UITabBarController?
            tabbarController?.selectedIndex = 3
        }
        else
        {
            print("recording not successful")
            recordButton.isEnabled=true
            stopButton.isHidden=true   
        }   
    }
    var currentUser: User?
    
    func customization() {
//        let icons = UIImage.init(named: "back")?.withRenderingMode(.alwaysOriginal)
//        let backButton = UIBarButtonItem.init(image: icons!, style: .plain, target: self, action: #selector(self.dismissSelf))
//        self.navigationItem.leftBarButtonItem = backButton
//        let icon = UIImage(named: "save")
//        let iconSize = CGRect(origin: CGPoint.zero, size: CGSize(width: 100, height: 15))
//        let iconButton = UIButton(frame: iconSize)
//        iconButton.setBackgroundImage(icon, for: .normal)
//        let barButton = UIBarButtonItem(customView: iconButton)
//        self.navigationItem.rightBarButtonItem = barButton
//        iconButton.addTarget(self, action: #selector(self.dismissSelf), for: .touchUpInside)
        //self.navigationController?.navigationBar.tintColor = UIColor.RGB(197, 104, 66)
        self.navigationItem.title = self.currentUser?.name
        self.profilePic.image = self.currentUser?.profilePic
        
    }
    
    func dismissSelf() {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        recordButton.isEnabled=true
        stopButton.isHidden=true
        statusLabel.text="00:00"
        volumeMeter.isHidden=true
        //profilePic.image = self.currentUser?.profilePic
        //cell.profilePic.image = self.currentUser?.profilePic
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 78/255, green: 78/255, blue: 78/255, alpha: 1)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        RecordLabel.text = "Start Recording"
        RecordLabel.sizeToFit();
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // For Timer
    func updateAudioMeter(_ timer:Timer) {
        
        if let recorder = self.audioRecorder {
            if recorder.isRecording {
                let min = Int(recorder.currentTime / 60)
                let sec = Int(recorder.currentTime.truncatingRemainder(dividingBy: 60))
                let s = String(format: "%02d:%02d", min, sec)
                statusLabel.text = s
                recorder.updateMeters()
                
            }
        }
    }
    
    // For voice recording indicator
    func updateMeter() {
        if self.peakPower > -30{
            DispatchQueue.main.async {
                self.volumeMeter.setProgress(0.5, animated: true)
            }
        }
        if self.peakPower > -20{
            DispatchQueue.main.async {
                self.volumeMeter.setProgress(0.6, animated: true)
            }
        }
        if self.peakPower > -10{
            DispatchQueue.main.async {
                self.volumeMeter.setProgress(0.7, animated: true)
            }
        }
        if self.peakPower > -5{
            DispatchQueue.main.async {
                self.volumeMeter.setProgress(0.8, animated: true)
            }
        }
        if self.peakPower > 0{
            DispatchQueue.main.async {
                self.volumeMeter.setProgress(0.85, animated: true)
            }
        }
        if self.peakPower < -30{
            DispatchQueue.main.async {
                self.volumeMeter.setProgress(0, animated: true)
            }
        }
    }
    

}


class RecordedAudio:NSObject
{
    var title:String!
    var filePathURL:NSURL!
}

