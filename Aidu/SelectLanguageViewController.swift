//
//  SelectLanguageViewController.swift
//  Aidu
//
//  Created by Haripriya on 10/5/17.
//  Copyright © 2017 Aidu. All rights reserved.
//

import Foundation

import UIKit

class SelectLanguageViewController: UITableViewController{
    //var checked = [Bool]();
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.accessoryType = .checkmark
        }
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.accessoryType = .none
        }
    }
    
    @IBAction func Done(_ sender: Any) {
        performSegue(withIdentifier: "done", sender: self)
        
    }
    
}

