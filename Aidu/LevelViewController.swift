//
//  LevelViewController.swift
//  Aidu
//
//  Created by varun murali on 12/07/17.
//  Copyright © 2017 Aidu. All rights reserved.
//

import UIKit


class LevelViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource {
    var levels=["", "Beginner" , "Intermediate","Advanced","Fluent"]
    var pickerLocation = 0;
    @IBOutlet weak var LevelPicker: UIPickerView!
    var profile = ProfileData()
    
    @IBOutlet weak var nextButton: UIButton!
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
            return 1;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            return levels[row];
        
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return levels.count;
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickerLocation=row;
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        
        return NSAttributedString(string: levels[row], attributes: [NSForegroundColorAttributeName:UIColor.white])
    }
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LevelPicker.delegate=self;
        LevelPicker.dataSource=self;
        LevelPicker.isUserInteractionEnabled = false;
        self.nextButton.isHidden = true;
        if profile.L2 == "English"{
            self.English(self)
        }
        if profile.L2 == "Mandarin"{
            self.Mandarin(self)
        }
        
        if profile.level == "Beginner" {
            pickerLocation = 1
            LevelPicker.selectRow(pickerLocation, inComponent: 0, animated: true)
        }
        if profile.level == "Intermediate" {
            pickerLocation = 2
            LevelPicker.selectRow(pickerLocation, inComponent: 0, animated: true)
        }
        if profile.level == "Advanced" {
            pickerLocation = 3
            LevelPicker.selectRow(pickerLocation, inComponent: 0, animated: true)
        }
        if profile.level == "Fluent" {
            pickerLocation = 4
            LevelPicker.selectRow(pickerLocation, inComponent: 0, animated: true)
        }
        // Do any additional setup after loading the view.
    }
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func next(_ sender: Any) {
        if(pickerLocation == 1){
            profile.level = "Beginner"
        }
        if(pickerLocation == 2){
             profile.level = "Intermediate"
        }
        if(pickerLocation == 3){
            profile.level = "Advanced"
        }
        if(pickerLocation == 4){
            profile.level = "Fluent"
        }
        performSegue(withIdentifier: "next", sender: self)
        
    }
    
    
    @IBAction func Back(_ sender: Any) {
        performSegue(withIdentifier: "back", sender: self)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "next" {
            let controller = segue.destination as! SchoolViewController
            print("Prinitng profile level")
            print(profile.level)
            controller.profile = profile;
        }
        if segue.identifier == "level" {
            let controller = segue.destination as! LevelDataViewController
            
            controller.profile = profile;
        }
        if segue.identifier == "back" {
            let controller = segue.destination as! LanguageViewController
            
            controller.profile = profile;
        }
    }
    
    var b1White = true;
    var b2White = true;
    var onclicked = true;
    
    @IBOutlet weak var Englang: UIButton!
    @IBAction func English(_ sender: Any) {
        profile.L2 = "English";
        
        b1White = !b1White;
        if(b1White == false && b2White == false){
            Englang.backgroundColor = UIColor(red: 97.0 / 255.0, green: 61.0 / 255.0, blue: 156.0 / 255.0, alpha: 1.0);
            Englang.titleLabel!.textColor = UIColor.white;
            Mandlang.backgroundColor = UIColor.white
            profile.L2 = "English";
            b2White = true;
            b1White = false;
            Englang.titleLabel!.textColor = UIColor.white;
            if(onclicked){
                self.Englang.setTitleColor(UIColor.white, for: UIControlState.normal);
                self.Mandlang.setTitleColor(UIColor.black, for: UIControlState.normal);
                self.nextButton.isHidden = false;
            }
            
        }
        else{
            if(b1White == true){
                Englang.backgroundColor = UIColor.white;
                LevelPicker.isUserInteractionEnabled = false;
                LevelPicker.selectRow(0, inComponent:0, animated:true)
                if(onclicked){
                    self.Englang.setTitleColor(UIColor.black, for: UIControlState.normal);
                    self.nextButton.isHidden = true;
                }
            }
            else{
                Englang.backgroundColor = UIColor(red: 97.0 / 255.0, green: 61.0 / 255.0, blue: 156.0 / 255.0, alpha: 1.0);
                profile.L2 = "English";
                LevelPicker.isUserInteractionEnabled = true;
                if(onclicked){
                    self.Englang.setTitleColor(UIColor.white, for: UIControlState.normal);
                    self.nextButton.isHidden = false;
                }
            }
            
        }
    }
    
    @IBOutlet weak var Mandlang: UIButton!
    @IBAction func Mandarin(_ sender: Any) {
        profile.L2 = "Mandarin";
        
        b2White = !b2White;
        if(b2White == false && b1White == false){
            Mandlang.backgroundColor = UIColor(red: 97.0 / 255.0, green: 61.0 / 255.0, blue: 156.0 / 255.0, alpha: 1.0);
            Englang.backgroundColor = UIColor.white
            profile.L2 = "Mandarin";
            b2White = false;
            b1White = true;
            //Mandlang.titleLabel!.textColor = UIColor.white;
            if(onclicked){
                self.Mandlang.setTitleColor(UIColor.white, for: UIControlState.normal);
                self.Englang.setTitleColor(UIColor.black, for: UIControlState.normal);
                self.nextButton.isHidden = false;
            }
            
            
        }
        else{
            if(b2White == true){
                Mandlang.backgroundColor = UIColor.white;
                LevelPicker.isUserInteractionEnabled = false;
                LevelPicker.selectRow(0, inComponent:0, animated:true)
                if(onclicked){
                    self.Mandlang.setTitleColor(UIColor.black, for: UIControlState.normal);
                    self.nextButton.isHidden = true;
                }
            }
            else{
                Mandlang.backgroundColor = UIColor(red: 97.0 / 255.0, green: 61.0 / 255.0, blue: 156.0 / 255.0, alpha: 1.0);
                
                profile.L2 = "Mandarin";
                LevelPicker.isUserInteractionEnabled = true;
                if(onclicked){
                    self.Mandlang.setTitleColor(UIColor.white, for: UIControlState.normal);
                    self.nextButton.isHidden = false;
                }
            }
            
        }
    }
    
    @IBAction func DontKnowLevel(_ sender: Any) {
        performSegue(withIdentifier: "level", sender: self)
    }

    

}
