//
//  Intrest.swift
//  Aidu
//
//  Created by varun murali on 14/07/17.
//  Copyright © 2017 Aidu. All rights reserved.
//

import Foundation
class Intrest
{
    var title: String
    var isIntrested: Bool
    
    public init(title: String)
    {
        self.title = title
        self.isIntrested = false
    }
    
}
extension Intrest
{
    public class func getArtData() -> [Intrest]
    {
        return [
            Intrest(title: "Photography"),
            Intrest(title: "Sculpting"),
            Intrest(title: "Painting"),
            Intrest(title: "Modern Art"),
            Intrest(title: "Renaissance Art"),
            Intrest(title: "Poetry"),
            Intrest(title: "Novels"),
            Intrest(title: "Short stories"),
        ]
    }
    public class func getSportsData() -> [Intrest]
    {
        return [
            Intrest(title: "Basketball"),
            Intrest(title: "Soccer"),
            Intrest(title: "American football"),
            Intrest(title: "Baseball"),
            Intrest(title: "Hockey"),
            Intrest(title: "Car racing"),
            Intrest(title: "Rugby"),
            Intrest(title: "Golf")
        ]
    }
    
    public class func getMusicData() -> [Intrest]
    {
        return [
            Intrest(title: "American pop"),
            Intrest(title: "Mandopop (Mandarin pop)"),
            Intrest(title: "Cantopop (Cantonese pop)"),
            Intrest(title: "Rock"),
            Intrest(title: "Country"),
            Intrest(title: "Jazz"),
            Intrest(title: "Hip Hop"),
            Intrest(title: "Soul"),
            Intrest(title: "R&B"),
            Intrest(title: "K-Pop")
        ]
    }
    public class func getNatureData() -> [Intrest]
    {
        return [
            Intrest(title: "Animals"),
            Intrest(title: "Birds"),
            Intrest(title: "Trees"),
            Intrest(title: "Farm"),
            Intrest(title: "Parks"),
            Intrest(title: "Mountains")
        ]
    }
    
    public class func getTVData() -> [Intrest]
    {
        return [
            Intrest(title: "Science fiction"),
            Intrest(title: "Drama"),
            Intrest(title: "Fantasy"),
            Intrest(title: "Comedies"),
            Intrest(title: "Korean shows"),
            Intrest(title: "Chinese shows"),
            Intrest(title: "Japanese shows"),
            Intrest(title: "American shows")
        ]
    }
    public class func getDesignData() -> [Intrest]
    {
        return [
            Intrest(title: "Interial design"),
            Intrest(title: "Architecture"),
            Intrest(title: "Automobile"),
            Intrest(title: "Consumer products"),
            Intrest(title: "Apps & Web"),
            Intrest(title: "Games"),
            Intrest(title: "VR / AR"),
            Intrest(title: "Ecodesign"),
            Intrest(title: "Universal design")
            
        ]
    }
    public class func InitialData() -> [Intrest]
    {
        return [
            Intrest(title: "Magazine")
        ]
    }
    
    public class func getMovieData() -> [Intrest]
    {
        return [
            Intrest(title: "Scary movies"),
            Intrest(title: "Chinese movies"),
            Intrest(title: "Action movies"),
            Intrest(title: "Korean movies"),
            Intrest(title: "Comedy"),
            Intrest(title: "Documentaries"),
            Intrest(title: "Japanese movies"),
            Intrest(title: "Science fiction"),
            Intrest(title: "Fantasy"),
            Intrest(title: "Animation")
            
        ]
    }
    public class func getITData() -> [Intrest]
    {
        return [
            Intrest(title: "IoT (Internet of things)"),
            Intrest(title: "AI (Artificial inteligence)"),
            Intrest(title: "Self driving cars"),
            Intrest(title: "Machine learning"),
            Intrest(title: "Neuroscience"),
            Intrest(title: "Robots"),
            Intrest(title: "Outer space"),
            Intrest(title: "New energy")
            
        ]
    }
    public class func getBusinessData() -> [Intrest]
    {
        return [
            Intrest(title: "Investment"),
            Intrest(title: "Venture capital"),
            Intrest(title: "Trade"),
            Intrest(title: "Banking"),
            Intrest(title: "Finance"),
            Intrest(title: "Currencies"),
            Intrest(title: "Stock market")
        ]
    }
    public class func getPoliticsData() -> [Intrest]
    {
        return [
            Intrest(title: "American politics"),
            Intrest(title: "World politics"),
            Intrest(title: "Chinese politics"),
            Intrest(title: "Local politics"),
            Intrest(title: "Elections"),
            Intrest(title: "Human rights")
        ]
    }
    public class func getHealthData() -> [Intrest]
    {
        return [
            Intrest(title: "Running"),
            Intrest(title: "Nutrition"),
            Intrest(title: "Weight lifting"),
            Intrest(title: "Yoga"),
            Intrest(title: "Biking"),
            Intrest(title: "Dieting")
        ]
    }
    public class func getFashionData() -> [Intrest]
    {
        return [
            Intrest(title: "Clothes"),
            Intrest(title: "Shoes"),
            Intrest(title: "Apparel"),
            Intrest(title: "Industry"),
            Intrest(title: "Hats"),
            Intrest(title: "Sun-glasses"),
            Intrest(title: "Sneakers"),
            Intrest(title: "Leather goods")
            
        ]
    }
    public class func getLanguagesData() -> [Intrest]
    {
        return [
            Intrest(title: "Romance languages"),
            Intrest(title: "Asian languages"),
            Intrest(title: "Language learning"),
            Intrest(title: "African languages"),
            Intrest(title: "Language teaching"),
            Intrest(title: "Language tech")
            
        ]
    }
    public class func getTravelData() -> [Intrest]
    {
        return [
            Intrest(title: "Cruises"),
            Intrest(title: "Beaches"),
            Intrest(title: "Adventure travel"),
            Intrest(title: "Study abroad"),
            Intrest(title: "International travel"),
            Intrest(title: "Road trips"),
            Intrest(title: "Day trips"),
            Intrest(title: "Camping"),
            Intrest(title: "Sailing")
            
        ]
    }
    public class func getFoodData() -> [Intrest]
    {
        return [
            Intrest(title: "Thai food"),
            Intrest(title: "Dim sum"),
            Intrest(title: "Vietnamese food"),
            Intrest(title: "Italian food"),
            Intrest(title: "Mexican food"),
            Intrest(title: "Fine dining"),
            Intrest(title: "Taiwanese food"),
            Intrest(title: "Northern food"),
            Intrest(title: "Southern food"),
            Intrest(title: "French food")
        ]
    }
    
}
