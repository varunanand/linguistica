//
//  InfoPageViewController.swift
//  Aidu
//
//  Created by Haripriya on 10/5/17.
//  Copyright © 2017 Aidu. All rights reserved.
//

import Foundation

import UIKit

class InfoPageViewController: UITableViewController {
    var invisibleToggle = true;
    @IBOutlet weak var done: UIButton!
    @IBOutlet weak var visibleToggle: UIButton!
    
    @IBAction func visibleToggle(_ sender: Any) {
        if(invisibleToggle==false){
            visibleToggle.setImage(UIImage(named: "Invisible.png"), for: .normal)
            
            invisibleToggle=true;
        }
        else{
            
            visibleToggle.setImage(UIImage(named: "Visible.png"), for: .normal)
            invisibleToggle=false;
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func Next(_ sender: Any) {
        performSegue(withIdentifier: "next", sender: self)
        
    }
    
    @IBAction func Done(_ sender: Any) {
        performSegue(withIdentifier: "done", sender: self)
        
    }
    
    
    
}

