//
//  UserProfileViewController.swift
//  Aidu
//
//  Created by Elara on 2/6/18.
//  Copyright © 2018 Aidu. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseMessaging
import Firebase;

class UserProfileViewController: UIViewController, UITextFieldDelegate {
    
    var profile = ProfileData()
    var aiduUserObject = UsersOnTheMapView()
    var conversations = NSDictionary()
    var contacts = NSArray()
    var sentRequests = NSArray()
    var receivedRequests = NSArray()
    let ref = Database.database().reference()
    let userLogged=Auth.auth().currentUser!.uid;
    var editedL2 = ""
    var editedL1 = ""
    var editedCity = ""
    var editedState = ""
    var editedCountry = ""
    var editedBio = ""
    var editedhobbyList =  [String]()
    var editedinterestList =  [String]()
    var activeTextField: UITextField!
    var activeTextView: UITextView!
    var displayInterestList: String!
    var frameRect: CGRect!
    
    @IBOutlet weak var userNameField: UITextField!
   
    @IBOutlet weak var profilePic: UIImageView!
    
    @IBOutlet weak var L2Button: UIButton!
    
    @IBOutlet weak var L1Button: UIButton!
    
    @IBOutlet weak var universityField: UITextField!
    
    @IBOutlet weak var HometownButton: UIButton!
    
    @IBOutlet weak var interestListLabel: UILabel!
    
    @IBOutlet weak var crossCultureConnx: UILabel!
    
    @IBOutlet weak var bioField: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        displayTheUserProfile()
        populateTheAiduUserObject()
        
        // Gesture recogniser for keyboard to go away on tapping anywhere on page.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UserProfileViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        let center: NotificationCenter = NotificationCenter.default;
        center.addObserver(self, selector: #selector(keyboardDidShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        center.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
self.navigationController?.setNavigationBarHidden(true, animated: true)
       
        
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        print("viewDidDisappear")
        self.persistData()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //on edit completion of username update the profile object
    @IBAction func userNameEdit(_ sender: Any) {
        self.profile.name = self.userNameField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        self.aiduUserObject.userName = self.profile.name
        print("value of username changed to")
        print(self.aiduUserObject.userName!)
    }
    
    @IBAction func universityEdit(_ sender: Any) {
        self.profile.school = universityField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    
    @IBAction func homePage(_ sender: Any) {
        //write code to update values in the field to database
        print("Trying to send data back")
        
        
        performSegue(withIdentifier: "completeEdit", sender: self)
    }
    
    @IBAction func L2Selection(_ sender: Any) {
       
        performSegue(withIdentifier: "L2Selector", sender: self)
    }
    
    @IBAction func L1Selection(_ sender: Any) {
        performSegue(withIdentifier: "L1Selector", sender: self)
    }
    

    @IBAction func bioEdit(_ sender: Any) {
        performSegue(withIdentifier: "bio", sender: self)
    }
    
    @IBAction func homeTownSelection(_ sender: Any){
        performSegue(withIdentifier: "homeTown", sender: self)
    }
    
    @IBAction func editInterests(_ sender: Any) {
        performSegue(withIdentifier: "editInterests", sender: self)
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField
    }
    
    
    func keyboardDidShow(notification: Notification) {
        
        let info:NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let keyboardY = self.view.frame.size.height - keyboardSize.height
        
        let editingTextFieldY:CGFloat! = self.activeTextField?.frame.origin.y
        
        if self.view.frame.origin.y >= 0 {
            //Checking if the textfield is really hidden behind the keyboard
       // if(editingTextFieldY != nil && keyboardY != nil){
            if editingTextFieldY > keyboardY - 60 {
                UIView.animate(withDuration: 0.25, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                    self.view.frame = CGRect(x: 0, y: self.view.frame.origin.y - (editingTextFieldY! - (keyboardY - 140)), width: self.view.bounds.width,height: self.view.bounds.height)
                }, completion: nil)
            }
        //  }
            
        }
        
        
        
        
    }
    
    func keyboardWillHide(notification: Notification) {
        UIView.animate(withDuration: 0.25, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.view.frame = CGRect(x: 0, y: 0,width: self.view.bounds.width, height: self.view.bounds.height)}, completion: nil)
      
    }
    
    
    func displayTheUserProfile(){
        
        
        //Getting all the information from the database. Populate it into a Profile object. All the information will be required to repopuate the DB with the modified values.
        self.ref.child("User").child(self.userLogged).observeSingleEvent(of: .value, with: { (snapshot) in
            
            if snapshot.exists() {
                if let value = snapshot.value as? [String: AnyObject] {
                    //usrname
                    if(nil != (value["username"])){
                        self.profile.name = value["username"] as? String ?? ""
                        //setting username
                        self.userNameField.text = self.profile.name
                    }
                    else{
                        print("There is no username present")
                    }
                    //l1
                    if(nil != (value["L1"])){
                        if(self.editedL1 == ""){
                            self.profile.L1 = value["L1"] as? String ?? ""
                        }
                        else{
                            self.profile.L1 = self.editedL1
                            self.editedL1 = ""
                            
                        }
                       
                        self.L1Button.setTitle(self.profile.L1, for: [])
                    }
                    else{
                        print("There is no L1 present")
                    }
                    //email
                    if(nil != (value["email"])){
                        self.profile.email = value["email"] as? String ?? ""
                    }
                    else{
                        print("There is no email present")
                    }
                    //l2
                    if(nil != (value["L2"])){
                        if(self.editedL2 == ""){
                        self.profile.L2 = value["L2"] as? String ?? ""
                        }
                        else{
                           self.profile.L2 = self.editedL2
                           self.editedL2 = ""
                            
                        }
                         self.L2Button.setTitle(self.profile.L2, for: [])
                    }
                    else{
                        print("There is no L2 present")
                    }
                    //level
                    if(nil != (value["level"])){
                        self.profile.level = value["level"] as? String ?? ""
                    }
                    else{
                        print("There is no level present")
                    }
                    //school
                    if(nil != (value["school"])){
                        self.profile.school = value["school"] as? String ?? ""
                        
                    self.universityField.text = self.profile.school
        
                    }
                    else{
                        print("There is no school present")
                    }
                    //country
                    if(nil != (value["county"])){
                        if(self.editedCountry == ""){
                        self.profile.country = value["county"] as? String ?? ""
                        }
                        else{
                            self.profile.country = self.editedCountry
                            self.editedCountry = ""
                        }
                        
                    }
                    else{
                        print("There is no county present")
                    }
                    //state
                    if(nil != (value["state"])){
                        if(self.editedState == ""){
                        self.profile.state = value["state"] as? String ?? ""
                        }
                        else{
                         self.profile.state = self.editedState
                            self.editedState = ""
                        }
                    }
                    else{
                        print("There is no state present")
                    }
                    //city
                    if(nil != (value["city"])){
                        if(self.editedCity == ""){
                        self.profile.city = value["city"] as? String ?? ""
                        }
                        else{
                            self.profile.city = self.editedCity
                            self.editedCity = ""
                        }
                    }
                    else{
                        print("There is no city present")
                    }
                    //ownstory
                    if(nil != (value["OwnStory"])){
                        if(self.editedBio == ""){
                        self.profile.ownStory = value["OwnStory"] as? String ?? ""
                        }
                        else{
                           self.profile.ownStory = self.editedBio
                            self.editedBio = ""
                        }
                       
                        
                       
                        self.bioField.text = self.profile.ownStory
                    }
                    else{
                        print("There is no OwnStory present")
                    }
                    //picture
                    if(nil != (value["picture"])){
                        let pictureurl = value["picture"] as? String ?? ""
                        self.profile.picture = URL(string: pictureurl)
                        
                        //setting picture
                        if let url = NSURL(string: pictureurl) {
                            if let imagedata = NSData(contentsOf: url as URL) {
                                self.profilePic.image = UIImage(data: imagedata as Data)
                            }
                        }
                    }
                    else{
                        print("There is no picture present")
                    }
                    //hobby list
                    if(nil != (value["Hobby"])){
                        self.profile.intrest = (value["Hobby"] as? [String])!
                    }
                    else{
                        print("There is no Hobby list present")
                    }
                    //interest list
                    if(nil != (value["intrestList"])){
                        
                        if(self.editedinterestList == []){
                        self.profile.intestList = (value["intrestList"] as? [String])!
                        }
                        else{
                           self.profile.intestList = self.editedinterestList
                           self.editedinterestList = []
                        }
                        
                    }
                    else{
                        print("There is no intrestList present")
                    }
                    //conversations
                    if(nil != (value["conversations"])){
                        self.conversations = (value["conversations"])! as! NSDictionary
                    }
                    else{
                        print("There are no conversations present")
                    }
                    
                    print("printing hobbylist and interest list")
                    print(self.profile.intrest)
                    print(self.profile.intestList)
                    print(self.conversations)
                    
                    
                    //setting city and state
                    
                    self.HometownButton.setTitle(self.profile.city!+", "+self.profile.state!, for: [])
                    
                    
                    if(self.profile.intestList.count >= 3){
                        self.displayInterestList = self.profile.intestList[0] + " | " + self.profile.intestList[1] + " | " + self.profile.intestList[2] + " ... "
                    }
                    else if(self.profile.intestList.count >= 2){
                        self.displayInterestList = self.profile.intestList[0] + " | " + self.profile.intestList[1]
                    }
                    else if(self.profile.intestList.count >= 1){
                        self.displayInterestList = self.profile.intestList[0]
                    }
                    else {
                        self.displayInterestList = "No interest list to display"
                    }
                    print(self.displayInterestList)
                    self.interestListLabel.text = self.displayInterestList
                    
                    
                }
            }
        })
    }
    
    func populateTheAiduUserObject(){
        
        self.ref.child("AiduUsers").child(self.userLogged).observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                if let value = snapshot.value as? [String: AnyObject] {
                    //contacts
                    if(nil != (value["contacts"])){
                        self.contacts = (value["contacts"])! as! NSArray
                    }
                    else{
                        print("There are no contacts present")
                    }
                    //crossconnect points
                    if(nil != (value["crossConnectPoints"])){
                        self.aiduUserObject.crossConnectPoints = value["crossConnectPoints"] as? Int
                        self.crossCultureConnx.text = String(self.aiduUserObject.crossConnectPoints) 
                    }
                    else{
                        print("There are no crossConnectPoints present")
                    }
                    //face to face
                    if(nil != (value["faceToface"])){
                        self.aiduUserObject.facToface = value["faceToface"] as? String ?? ""
                    }
                    else{
                        print("There is no faceToface present")
                    }
                    //InAidu Area
                    if(nil != (value["inAiduArea"])){
                        self.aiduUserObject.inAiduArea = value["inAiduArea"] as? Bool
                    }
                    else{
                        print("There is no inAiduArea present")
                    }
                    //l1
                    if(nil != (value["l1"])){
                        self.aiduUserObject.l1 = value["l1"] as? String ?? ""
                    }
                    else{
                        print("There is no l1 present")
                    }
                    //l2
                    if(nil != (value["l2"])){
                        self.aiduUserObject.l2 = value["l2"] as? String ?? ""
                    }
                    else{
                        print("There is no l2 present")
                    }
                    //latitude
                    if(nil != (value["latitude"])){
                        self.aiduUserObject.latitude = value["latitude"] as? Double
                    }
                    else{
                        print("There is no latitude present")
                    }
                    
                    //longitude
                    if(nil != (value["longitude"])){
                        self.aiduUserObject.longitude = value["longitude"] as? Double
                    }
                    else{
                        print("There is no longitude present")
                    }
                    
                    //name
                    if(nil != (value["name"])){
                        self.aiduUserObject.userName = value["name"] as? String ?? ""
                    }
                    else{
                        print("There is no name present")
                    }
                    
                    //visible
                    if(nil != (value["visible"])){
                        self.aiduUserObject.visible = value["visible"] as? Bool
                    }
                    else{
                        print("There is no visible present")
                    }
                    
                    //sentRequests
                    if(nil != (value["sentRequests"])){
                        self.sentRequests = (value["sentRequests"])! as! NSArray
                    }
                    else{
                        print("There is no sentRequests present")
                    }
                    
                    //receivedRequests
                    if(nil != (value["receivedRequests"])){
                        self.receivedRequests = (value["receivedRequests"])! as! NSArray
                    }
                    else{
                        print("There is no receivedRequests present")
                    }
                    
                }
            }
        })
    }
    //Calls this function when the tap is recognized.
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func persistData(){
        //let ref = Database.database().reference()
        self.ref.child("User").child(self.userLogged).setValue(["username": self.profile.name, "email" : self.profile.email, "L1" : self.profile.L1! ,"L2": self.profile.L2 , "city" : self.profile.city! ,"county" : self.profile.country , "level" : self.profile.level! , "Hobby" : self.profile.intrest , "intrestList" : self.profile.intestList , "OwnStory" : self.profile.ownStory , "school" : self.profile.school , "picture": self.profile.picture?.absoluteString , "state" : self.profile.state! , "conversations": self.conversations]);
        
        self.ref.child("AiduUsers").child(self.userLogged).setValue(["contacts": self.contacts, "crossConnectPoints" : self.aiduUserObject.crossConnectPoints, "faceToface" : self.aiduUserObject.facToface! ,"inAiduArea": self.aiduUserObject.inAiduArea , "l1" : self.profile.L1 ,"l2" : self.profile.L2 , "latitude" : self.aiduUserObject.latitude! , "longitude" : self.aiduUserObject.longitude , "name" : self.aiduUserObject.userName , "visible" : self.aiduUserObject.visible , "sentRequests" : self.sentRequests , "receivedRequests": self.receivedRequests]);
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "L2Selector" {
        
            if(profile.L2 == "English"){
                let controller = segue.destination as! L2SelectorViewController
                controller.selected = 1;
            }
            else{
                let controller = segue.destination as! L2SelectorViewController
                controller.selected = 0;
            }
            
        }
        
        
        if segue.identifier == "L1Selector" {
            
            if(profile.L1 == "English"){
                let controller = segue.destination as! L1SelectorViewController
                controller.selected = 1;
            }
            else{
                let controller = segue.destination as! L1SelectorViewController
                controller.selected = 0;
            }
            
        }
        
        if segue.identifier == "homeTown" {
            
           
                let controller = segue.destination as! HomeTownViewController
                controller.country = self.profile.country;
                controller.city = self.profile.city
                controller.state = self.profile.state
            
            
            
        }
        if segue.identifier == "bio" {
            let controller = segue.destination as! BioViewController
            controller.bio = self.profile.ownStory
          
        }
        
        if segue.identifier == "editInterests" {
            let controller = segue.destination as! EditInterestTableViewController
            
            controller.myHobbyList = self.profile.intrest
            controller.myInterestList = self.profile.intestList
            
        }
    }
    
}
