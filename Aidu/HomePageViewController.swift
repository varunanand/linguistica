//
//  HomePageViewController.swift
//  Aidu
//
//  Created by varun murali on 14/07/17.
//  Copyright © 2017 Aidu. All rights reserved.
//

import UIKit
import MapKit;
import CoreLocation
import Firebase;
class HomePageViewController: UIViewController, MKMapViewDelegate,CLLocationManagerDelegate {
    
    @IBOutlet weak var profileButton: UIButton!
    @IBAction func Recenter(_ sender: Any) {
        self.mapView.setUserTrackingMode( MKUserTrackingMode.follow, animated: true)
        
    }
    @IBOutlet weak var filterToggle: UISegmentedControl!
    @IBAction func mapFilter(_ sender:UISegmentedControl) {
        switch filterToggle.selectedSegmentIndex {
        case 0:
            fetchUsers()
          // print("1")
            break
        case 1:
          // print("2")
           fetchL2Users()
            break
        default:
           // print("3")
            fetchNativeUsers()
            break;
        }
        
    }
    var profile = ProfileData();
    var invisible = false;
    var inAidu = true;
    @IBOutlet weak var mapView: MKMapView!
    let manager = CLLocationManager();
    var users = [UsersOnTheMapView]();
    var userLocations = [CLLocation]();
    var contactsList = [String]();
    let locationManager = CLLocationManager();
    var languageFilter = "English";
    var isAiduFlag : Bool?;
    var isVisibleFlag : Bool?;
    var location = CLLocation();
    var currentSelectedUserOnMapView: String?
    var entryChecker = true;
    var userLogged : String?;
    var invisibleToggle = true;
    var ref: DatabaseReference!
    
//    UISwitch *Toggle = [[UISwitch alloc] init];
//    Toggle.transform = CGAffineTransformMakeScale(0.75, 0.75);
//    
    
    
    
    
    @IBAction func Info(_ sender: Any) {
        performSegue(withIdentifier: "next", sender: self)
        
    }
    
    
    
    @IBOutlet weak var visibileToggle: UIButton!
    
    @IBAction func visibleToggle(_ sender: Any) {
        userLogged=Auth.auth().currentUser!.uid;
        print("the logged in user is",userLogged);
        
        if(invisibleToggle==false){
            visibileToggle.setImage(UIImage(named: "Invisible.png"), for: .normal)
          self.ref.child("AiduUsers").child(userLogged!).child("visible").setValue(false)
            invisibleToggle=true;
        }
        else{
            self.ref.child("AiduUsers").child(userLogged!).child("visible").setValue(true)
            visibileToggle.setImage(UIImage(named: "Visible.png"), for: .normal)
            invisibleToggle=false;
        }
        
    }
    
    @IBAction func visibilityToggle(_ sender: Any) {
    
        
        if Auth.auth().currentUser != nil {
       //     print("logged in")
            userLogged=Auth.auth().currentUser?.uid;
       //     print("the logged user is" , userLogged)
            
        } else {
      //      print("not logged in");
        }
                invisible = !invisible ;
        if(invisible == false){
            
            
        }
        else{
          
            print("The logged user is" )
         Database.database().reference().child("AiduUsers").child(userLogged!).child("visible").setValue(false)
            
        }
        
    }
    
    
    @IBAction func userProfile(_ sender: Any) {
        
        performSegue(withIdentifier: "userProfile", sender: self)
        
        
    }
    
    func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion) {
        if Auth.auth().currentUser != nil {
        //    print("logged in")
            userLogged=Auth.auth().currentUser?.uid;
            print("the logged user is" , userLogged)
            
        }
     //   print("I am in determine state")
        // only notify user if user is still within this region.
        
        if state == CLRegionState.inside{
      //      print(" i am inside aidu region")
             Database.database().reference().child("AiduUsers").child(userLogged!).child("inAiduArea").setValue(true)
            
        }
        else{
       //     print(" i am outside aidu region")
                Database.database().reference().child("AiduUsers").child(userLogged!).child("inAiduArea").setValue(false)
            let alert = UIAlertController(title: "Out of Aidu Area", message: "You are out of AiduArea.To protect your safety, you can only view Aidu users within Aidu area", preferredStyle: UIAlertControllerStyle.alert)
            
           
           
            // show the alert
            
            let CMU:CLLocationCoordinate2D = CLLocationCoordinate2DMake(40.44534,-79.9491);
            let span : MKCoordinateSpan = MKCoordinateSpanMake(0.1, 0.1);
            let region : MKCoordinateRegion = MKCoordinateRegionMake(CMU, span);
            mapView.setRegion(region, animated: true);
            inAidu = false;
          //  print("i am going back to cmu",region)
            self.present(alert, animated: true, completion: nil)
            alert.addAction(UIAlertAction(title: "Find Aidu Users", style: UIAlertActionStyle.default, handler: nil))
            
        
        }
    }
    func applicationDidEnterBackground(_ application: UIApplication) {
       
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location =  locations[0];
        let span : MKCoordinateSpan = MKCoordinateSpanMake(0.01, 0.01);
        
        let myLocation : CLLocationCoordinate2D = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
        userLogged=Auth.auth().currentUser?.uid;
        let region : MKCoordinateRegion = MKCoordinateRegionMake(myLocation, span);
        //mapView.setRegion(region, animated: true);
        locationManager.stopUpdatingLocation()
        self.mapView.showsUserLocation = true;
    
       // print("lat and long is 8765" ,location.coordinate.longitude , location.coordinate.latitude)
        Database.database().reference().child("AiduUsers").child(userLogged!).child("longitude").setValue(location.coordinate.longitude);
         Database.database().reference().child("AiduUsers").child(userLogged!).child("latitude").setValue(location.coordinate.latitude);
        
        //self.manager.stopUpdatingLocation()
        
    }
    
    
    
    
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
         userLogged=Auth.auth().currentUser?.uid;
        Database.database().reference().child("AiduUsers").child(userLogged!).child("inAiduArea").setValue("true")
        entryChecker = true;
    }
    
    
    func locationManager(_ manager: CLLocationManager, didExitRegion: CLRegion) {
         userLogged=Auth.auth().currentUser?.uid;
        Database.database().reference().child("AiduUsers").child(userLogged!).child("inAiduArea").setValue("false")
    //    print("Hi he left")
        entryChecker = false;
        
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: NSError) {
     //   print("error:: (error)")
    }
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        let identifier = "mypin"
     
        if annotation is PersonLocator {
            var person = annotation as! PersonLocator
      //      print("person" , person.title);
       //     print("native" , person.isNative);
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                annotationView!.canShowCallout = true
                if(person.isContact == true && person.isNative == true){
                    let pinImage = UIImage(named: "NativeContact.png");
                    let size = CGSize(width: 30, height: 30)
                    UIGraphicsBeginImageContext(size)
                    pinImage?.draw(in: CGRect(origin: CGPoint(x: 0,y :0), size: size))
                    let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
                    UIGraphicsEndImageContext()
                    annotationView?.image = resizedImage;
                }
                else if(person.isContact == true && person.isNative == false)
                {
                    let pinImage = UIImage(named: "L2Contact.png");
                    let size = CGSize(width: 30, height: 30)
                    UIGraphicsBeginImageContext(size)
                    pinImage?.draw(in: CGRect(origin: CGPoint(x: 0,y :0), size: size))
                    let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
                    UIGraphicsEndImageContext()
                    annotationView?.image = resizedImage;
                }
                else if(person.isContact == false && person.isNative == true)
                {
                    let pinImage = UIImage(named: "NativeNonContact.png");
                    let size = CGSize(width: 30, height: 30)
                    UIGraphicsBeginImageContext(size)
                   pinImage?.draw(in: CGRect(origin: CGPoint(x: 0,y :0), size: size))
                    let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
                    UIGraphicsEndImageContext()
                    annotationView?.image = resizedImage;
                    //annotationView?.image = UIImage(named: "NativeNonContact.png")
                }
                else if(person.isContact == false && person.isNative == false)
                {
                    let pinImage = UIImage(named: "L2NonContact.png");
                    let size = CGSize(width: 30, height: 30)
                    UIGraphicsBeginImageContext(size)
                    pinImage?.draw(in: CGRect(origin: CGPoint(x: 0,y :0), size: size))
                    let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
                    UIGraphicsEndImageContext()
                    annotationView?.image = resizedImage;
                    //annotationView?.image = UIImage(named: "L2NonContact.png")
                }
                let btn = UIButton(type: .detailDisclosure)
                annotationView!.rightCalloutAccessoryView = btn
           
            return annotationView
        }
        return nil;
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        let person = view.annotation as! PersonLocator
        let personName = person.title
        let personId = person.personId
        self.currentSelectedUserOnMapView = personId
        if(self.users.count > 0){
            self.performSegue(withIdentifier: "profile", sender: self)
        }
        //let ac = UIAlertController(title: personName, message: personId, preferredStyle: .alert)
        //ac.addAction(UIAlertAction(title: "OK", style: .default))
        //present(ac, animated: true)
        
    }
    
    
    
    
    
 
    
    func sortUsersBasedOnProximity()
    {
        if(users.count>0){
            for object in users {
                object.distanceFromViewer = location.distance(from: object.currentUserLocation!);
            }
            
            users.sort(by: {
                $0.distanceFromViewer < $1.distanceFromViewer
            })
            
        }
    }
    
    func printUser()
    {
        if(users.count>0){
            for object in users {
             //   print(object.userName, object.distanceFromViewer);
            }
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //Getting users Native language
        let userId=Auth.auth().currentUser!.uid;
        ref = Database.database().reference()
        print(userId);
        var myUser = userId as? String
        var tokenVal = InstanceID.instanceID().token()
        let myToken: [String: AnyObject?] = [myUser!: tokenVal as AnyObject];
        ref.child("tokens").updateChildValues(myToken);
        
        ref.child("User").child(userId).observeSingleEvent(of: .value, with: { snapshot in
            
            if !snapshot.exists() {
                
            //    print("No snapshot")
            }
            if let value = snapshot.value as? [String: AnyObject] {
            //   print("the language has been set")
            var L1 = value["L1"] as? String ?? "";
            var pictureurl = value["picture"] as? String ?? ""

            self.languageFilter=L1;
          //  print("the set language is" + self.languageFilter)
                
                //code to get profile pic in the ui button
                if let url = URL(string: pictureurl) {
                    
            self.downloadImage(url: url)
                }
            }
            
            
        });
        
        
       // profileButton.frame = CGRect(x: 160, y: 100, width: 50 , height: 50)
       profileButton.layer.cornerRadius = 0.3 * profileButton.bounds.size.width
        profileButton.clipsToBounds = true
        
        
        
        self.mapView.delegate = self;
        manager.delegate=self
        let title = "CMU"
     //   print("i have requested location");
        manager.desiredAccuracy=kCLLocationAccuracyBest
        manager.requestAlwaysAuthorization();
        manager.startUpdatingLocation()
        //manager.stopUpdatingLocation()
        manager.startMonitoringSignificantLocationChanges();
        
        let coordinate = CLLocationCoordinate2DMake(40.4410936 , -79.9422009)
        let regionRadius = 100.0

     
        manager.requestState(for:CLCircularRegion(center: CLLocationCoordinate2D(latitude: coordinate.latitude,
                                                                                         longitude: coordinate.longitude), radius: regionRadius, identifier: title)
        );
        fetchUsers()
        setupData()
    
        
        
        
        
    }

    func setupData() {
        // 1. check if system can monitor regions
        if CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
            
            // 2. region data
            let title = "CMU"
            let coordinate = CLLocationCoordinate2DMake(40.442775 , -79.942721)
            let regionRadius = 1000.0
            
            // 3. setup region
            let region = CLCircularRegion(center: CLLocationCoordinate2D(latitude: coordinate.latitude,
                                                                         longitude: coordinate.longitude), radius: regionRadius, identifier: title)
            
            
            
        //    print("hi i am in setup data")
            manager.startMonitoring(for: region)
            
            // 4. setup annotation
            let restaurantAnnotation = MKPointAnnotation()
            restaurantAnnotation.coordinate = coordinate;
            restaurantAnnotation.title = "\(title)";
  
            
        
        }
        else {
       //     print("System can't track regions")
        }
    }
    
    // 6. draw circle
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        let circleRenderer = MKCircleRenderer(overlay: overlay)
        circleRenderer.strokeColor = UIColor.red
        circleRenderer.lineWidth = 1.0
        return circleRenderer
    }
    
    
    func fetchNativeUsers()
    {
        let allAnnotations = self.mapView.annotations
        self.mapView.removeAnnotations(allAnnotations)
        Database.database().reference().child("AiduUsers").observe(.childAdded, with: {
            (snapshot) in
            let dataDict =  snapshot.value as! [String: AnyObject]
            print(dataDict);
            let user = UsersOnTheMapView()
            var isContactFlag: Bool!;
            var isNativeFlag: Bool!;
          //  print("The users are",user.inAiduArea,user.longitude)
            user.userId = snapshot.key;
            user.l1 = dataDict["l1"] as? String;
            user.l2 = dataDict["l2"] as? String;
            user.latitude = dataDict["latitude"] as! Double;
            user.longitude = dataDict["longitude"] as! Double;
            user.inAiduArea = dataDict["inAiduArea"] as! Bool;
            user.visible = dataDict["visible"] as! Bool;
            user.userName = dataDict["name"] as! String;
            user.currentUserLocation = CLLocation(latitude: user.latitude, longitude: user.longitude);
            
            self.sortUsersBasedOnProximity();
            
            //self.printUser();
            
          DispatchQueue.main.async(execute: {
            
            if(user.l1 == self.languageFilter){
          //      print("i am a native" + user.userName!)
                isNativeFlag = true;
            }
            else{
          //      print("i am a L2" + user.userName!)
                isNativeFlag = false;
            }
            if(user.inAiduArea){
                self.isAiduFlag = true;
            }
            else{
                self.isAiduFlag = false;
            }
            if(user.visible){
                self.isVisibleFlag = true;
            }
            else{
                self.isVisibleFlag = false;
            }
            if(ContactsReaderForMap.sharedContacts.contactsList.count>0){
                for object in ContactsReaderForMap.sharedContacts.contactsList {
                    let contact = object?.contactId;
                    
                    if(contact == user.userId){
                        //print(user.userName, "is a contact");
                        isContactFlag = true;
                    }
                    else{
                        //print(user.userName, "is not a contact");
                        isContactFlag = false;
                    }
                }
            }
            print("contact is" , isContactFlag)
           
            let userLoggedIn=Auth.auth().currentUser!.uid;
            isContactFlag = true; // We need to Make sure this needs to be removed
         //   print("isNativeFlag for ",user.userName ," is " ,isNativeFlag)
            
            if(self.isAiduFlag == true && self.isVisibleFlag == true && userLoggedIn != user.userId && isNativeFlag == true){
          //      print("isNativeFlag for ",user.userName ,"is " ,isNativeFlag)
          //      print("Checking how many users are added")
                let annotatedPerson = PersonLocator(title: user.userName!, coordinate: CLLocationCoordinate2D(latitude: user.latitude!, longitude: user.longitude), personId: user.userId!, isContact: isContactFlag!, isNative: isNativeFlag!)
                self.users.append(user);
                self.mapView.addAnnotation(annotatedPerson)
                self.mapView.showAnnotations(self.mapView.annotations, animated: true)
                
            }
        })
            
            
            
            
            /*print(snapshot.key)*/}, withCancel: nil)
    }
    
    func fetchL2Users()
    {
        let allAnnotations = self.mapView.annotations
        self.mapView.removeAnnotations(allAnnotations)
        
        Database.database().reference().child("AiduUsers").observe(.childAdded, with: {
            (snapshot) in
            let dataDict =  snapshot.value as! [String: AnyObject]
            print(dataDict);
            let user = UsersOnTheMapView()
            var isContactFlag: Bool!;
            var isNativeFlag: Bool!;
            user.userId = snapshot.key;
            user.l1 = dataDict["l1"] as? String;
            user.l2 = dataDict["l2"] as? String;
            user.latitude = dataDict["latitude"] as! Double;
            user.longitude = dataDict["longitude"] as! Double;
            user.inAiduArea = dataDict["inAiduArea"] as! Bool;
            user.visible = dataDict["visible"] as! Bool;
            user.userName = dataDict["name"] as! String;
            user.currentUserLocation = CLLocation(latitude: user.latitude, longitude: user.longitude);
            self.sortUsersBasedOnProximity();
            
            //self.printUser();
            
            DispatchQueue.main.async(execute: {
            if(user.l1 == self.languageFilter){
          //      print("i am a native" + user.userName!)
                isNativeFlag = true;
            }
            else{
          //      print("i am a L2" + user.userName!)
                isNativeFlag = false;
            }
            if(user.inAiduArea){
                self.isAiduFlag = true;
            }
            else{
                self.isAiduFlag = false;
            }
            if(user.visible){
                self.isVisibleFlag = true;
            }
            else{
                self.isVisibleFlag = false;
            }
            if(ContactsReaderForMap.sharedContacts.contactsList.count>0){
                for object in ContactsReaderForMap.sharedContacts.contactsList {
                    let contact = object?.contactId;
                    
                    if(contact == user.userId){
                        //print(user.userName, "is a contact");
                        isContactFlag = true;
                    }
                    else{
                        //print(user.userName, "is not a contact");
                        isContactFlag = false;
                    }
                }
            }
            let userLoggedIn=Auth.auth().currentUser!.uid;
            isContactFlag = true; // We need to Make sure this needs to be removed
            
            if(self.isAiduFlag == true && self.isVisibleFlag == true && userLoggedIn != user.userId && isNativeFlag == false){
                let annotatedPerson = PersonLocator(title: user.userName!, coordinate: CLLocationCoordinate2D(latitude: user.latitude!, longitude: user.longitude), personId: user.userId!, isContact: isContactFlag!, isNative: isNativeFlag!)
           //     print("Checking how many users are added")
                self.users.append(user);
                self.mapView.addAnnotation(annotatedPerson)
                self.mapView.showAnnotations(self.mapView.annotations, animated: true)
            }
            
        })
            
            
            
            
            /*print(snapshot.key)*/}, withCancel: nil)
    }
    
    
    func downloadImage(url: URL) {
        print("Download Started")
        getDataFromUrl(url: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            DispatchQueue.main.async() {
                //add the profile pic to the button here
                self.profileButton.setBackgroundImage(UIImage(data: data), for: UIControlState.normal)

               // self.imageView.image = UIImage(data: data)
            }
        }
    }
    func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            }.resume()
    }
    func fetchUsers()
    {
        let allAnnotations = self.mapView.annotations
        self.mapView.removeAnnotations(allAnnotations)
        
        Database.database().reference().child("AiduUsers").observe(.childAdded, with: {
            (snapshot) in
            let dataDict =  snapshot.value as! [String: AnyObject]
         //   print(dataDict);
            let user = UsersOnTheMapView()
            var isContactFlag: Bool!;
            var isNativeFlag: Bool!;
         //   print("The users are",user.inAiduArea,user.longitude)
            user.userId = snapshot.key;
            user.l1 = dataDict["l1"] as? String;
            user.l2 = dataDict["l2"] as? String;
            user.latitude = dataDict["latitude"] as! Double;
            user.longitude = dataDict["longitude"] as! Double;
            user.inAiduArea = dataDict["inAiduArea"] as! Bool;
            user.visible = dataDict["visible"] as! Bool;
            user.userName = dataDict["name"] as! String;
            user.currentUserLocation = CLLocation(latitude: user.latitude, longitude: user.longitude);
            self.sortUsersBasedOnProximity();
            
            //self.printUser();
            
            DispatchQueue.main.async(execute: {
                if(user.l1 == self.languageFilter){
            //        print("i am a native" + user.userName!)
                    isNativeFlag = true;
                }
                else{
              //      print("i am a L2" + user.userName!)
                    isNativeFlag = false;
                }
                if(user.inAiduArea){
                    self.isAiduFlag = true;
                }
                else{
                    self.isAiduFlag = false;
                }
                if(user.visible){
                    self.isVisibleFlag = true;
                }
                else{
                    self.isVisibleFlag = false;
                }
                if(ContactsReaderForMap.sharedContacts.contactsList.count>0){
                    for object in ContactsReaderForMap.sharedContacts.contactsList {
                        let contact = object?.contactId;
                        
                        if(contact == user.userId){
                            //print(user.userName, "is a contact");
                            isContactFlag = true;
                        }
                        else{
                            //print(user.userName, "is not a contact");
                            isContactFlag = false;
                        }
                    }
                }
           //     print("contact is" , isContactFlag)
           //     print("isNativeFlag",isNativeFlag)
                let userLoggedIn=Auth.auth().currentUser!.uid;
                isContactFlag = true; // We need to Make sure this needs to be removed
                
                if(self.isAiduFlag! && self.isVisibleFlag! && userLoggedIn != user.userId){
                    let annotatedPerson = PersonLocator(title: user.userName!, coordinate: CLLocationCoordinate2D(latitude: user.latitude!, longitude: user.longitude), personId: user.userId!, isContact: isContactFlag!, isNative: isNativeFlag!)
             //       print("Checking how many users are added")
                    self.users.append(user);
                    self.mapView.addAnnotation(annotatedPerson)
                    self.mapView.showAnnotations(self.mapView.annotations, animated: true)
                }
            })
            
                
            
            
            
            
            /*print(snapshot.key)*/}, withCancel: nil)
        
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "profile" {
            let controller = segue.destination as! ProfilePageViewController
            controller.users = self.users;
            controller.currentUser = self.currentSelectedUserOnMapView;
        }
        
        
    }
    
}


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */


