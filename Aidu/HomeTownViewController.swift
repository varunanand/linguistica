//
//  HomeTownViewController.swift
//  Aidu
//
//  Created by Elara on 2/14/18.
//  Copyright © 2018 Aidu. All rights reserved.
//

import UIKit

class HomeTownViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
    
    var country: String!
    var city: String!
    var state: String!
    
    let countries:[String] = ["USA", "China", "Other"]
    
    @IBOutlet weak var countryLabel: UILabel!
    
    @IBOutlet weak var cityField: UITextField!
    
    @IBOutlet weak var countryPicker: UIPickerView!
    
    @IBOutlet weak var stateField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        countryLabel.text = country
        cityField.text = city
        stateField.text = state
        
        if(country == "USA"){
            countryPicker.selectRow(0, inComponent: 0, animated: true)
        }
        else if (country == "China"){
            
            countryPicker.selectRow(1, inComponent: 0, animated: true)
        }
        else{
            countryPicker.selectRow(2, inComponent: 0, animated: true)
        }
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func DoneButton(_ sender: Any) {
        city = cityField.text
        state = stateField.text
        
        performSegue(withIdentifier: "doneHomeTown", sender: self)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
            countryLabel.text = countries[row]
            country = countries[row]
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countries.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return countries[row]
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "doneHomeTown" {
            
           
                let controller = segue.destination as! UserProfileViewController
                controller.editedCity = city
                controller.editedCountry = country
                controller.editedState = state
            
            
        }
        
       
        
    }
}
