//
//  TableViewDelegate.swift
//  Autocomplete
//
//  Created by Amir Rezvani on 3/6/16.
//  Copyright © 2016 cjcoaxapps. All rights reserved.
//  Extended by Sriram Jaikrishnan 2/18/18 -  Current version developed by Amir only supports autocomplete of a field in a screen. Extended it to support autocomplete for two fields

import UIKit

extension AutoCompleteViewController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.cellHeight!
    }

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedItem = self.autocompleteItems![(indexPath as NSIndexPath).row]
        if self.activeTextField == self.textFieldState{
            self.textFieldState?.text = selectedItem.text}
        else if self.activeTextField == self.textFieldCity{
            self.textFieldCity?.text = selectedItem.text
        }
        UIView.animate(withDuration: self.animationDuration, animations: { () -> Void in
                self.view.frame.size.height = 0.0
            if self.activeTextField == self.textFieldState{
                self.textFieldState?.endEditing(true)}
            else if self.activeTextField == self.textFieldCity{
                self.textFieldCity?.endEditing(true)
            }
            
            }, completion: { (completed) -> Void in
                self.delegate!.didSelectItem(selectedItem)
        }) 
    }
}
